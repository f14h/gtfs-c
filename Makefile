TARGET 	:= gtfs
SHELL	:=/bin/bash

CFILES 		:= $(wildcard src/*.c)
CPPFILES 	:= $(wildcard src/*.cpp)

OBJ := $(CFILES:.c=.o)
OBJ += $(CPPFILES:.cpp=.o)

DEP := $(OBJ:.o=.d)

include ./Makefile.var

CFLAGS +=
CXXFLAGS +=

.SILENT:
.PHONY: all clean
all: $(TARGET)

-include $(DEP)

%.o: %.c
	echo [CC] $@
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ -c $<

%.o: %.cpp
	echo [CXX] $@
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $<

$(TARGET): $(OBJ)
	echo [LD] $@
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	
check:
	$(MAKE) -C tests check

clean:
	rm -f $(DEP)
	rm -f $(OBJ)
	rm -f $(TARGET)
	$(MAKE) -C tests clean
	
