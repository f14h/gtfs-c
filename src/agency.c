#include "agency.h"

#include <assert.h>     // for assert
#include <csv.h>        // for csv_error, csv_fini, csv_free, csv_init, csv_...
#include <string.h>     // for memset
#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "hashtable.h"  // for hashtable_init, hashtable_set, HT_FREE_VALUE
#include "utils.h"      // for estrdup, efree, ecalloc, get_index, LIBCSV_CB1

#include "debug.h"

const char *AGENCY_FIELDS[] = 
{
	"NULL",
	"agency_id",
	"agency_name",
	"agency_url",
	"agency_timezone",
	"agency_lang",
	"agency_phone",
	"agency_fare_url",
	"agency_email"
};

typedef struct
{
	size_t field;
	ssize_t row_cntr;
	agency_t *agency;
	const gtfs_database_t *db;
	agency_fields_t cols_map[agency_fields_t_NUM_MEMBERS];
} agency_csv_parser_data_t;

void agency_print_compact(const char *title, const agency_t *agency, const char *fallback)
{
	if (agency == NULL)
		printf("%s: %s\n", title, fallback);
	else
		printf("%s: %s\n", title, agency->name);
}

void agency_print(const agency_t *agency)
{
	#pragma omp critical(agency_print)
	{
		assert(agency != NULL);
		
		printf("--------------------\n");
		printf("agency_id: %s\n", agency->id);
		printf("agency_name: %s\n", agency->name);
		printf("agency_url: %s\n", agency->url);
		printf("agency_timezone: %s\n", agency->timezone);
		printf("agency_lang: %s\n", agency->lang);
		printf("agency_phone: %s\n", agency->phone);
		printf("agency_fare_url: %s\n", agency->fare_url);
		printf("agency_email: %s\n", agency->email);
		printf("--------------------\n");
	}
}

int agency_cmp(agency_t * const *agency1, agency_t * const *agency2)
{
	if (*agency1 == NULL && *agency2 == NULL) return 0;
	if (*agency1 == NULL) return -1;
	if (*agency2 == NULL) return 1;
	
	if (*agency1 == *agency2) return 0;
	
	return strcmp((*agency1)->name, (*agency2)->name);
}

void agency_free(agency_t **agency)
{
	agency_t *ag;
	
	assert(agency != NULL);
	assert(*agency != NULL);
	
	ag = *agency;
		
	efree(ag->id);
	efree(ag->name);
	efree(ag->url);
	efree(ag->timezone);
	efree(ag->lang);
	efree(ag->phone);
	efree(ag->fare_url);
	efree(ag->email);
	efree(ag);
	
	*agency = NULL;
}

static void cb1(char *field, size_t size, agency_csv_parser_data_t *p)
{
	ssize_t index;
	
	if (p->row_cntr == 0)
	{
		index = get_index(AGENCY_FIELDS, field, agency_fields_t_NUM_MEMBERS);
		if (index >= 0)
		{
			p->cols_map[p->field] = (agency_fields_t) index;
		}
		else
			fprintf(stderr, "Error reading AGENCY file: Unknown header <%s>\n", field);
	}
	else
	{
		switch (p->cols_map[p->field])
		{
			case AGENCY_ID:
				p->agency->id = estrdup(field);
				break;
				
			case AGENCY_NAME:
				p->agency->name = estrdup(field);
				break;
				
			case AGENCY_URL:
				p->agency->url = estrdup(field);
				break;
			
			case AGENCY_TIMEZONE:
				p->agency->timezone = estrdup(field);
				break;
			
			case AGENCY_LANG:
				p->agency->lang = estrdup(field);
				break;
				
			case AGENCY_PHONE:
				p->agency->phone = estrdup(field);
				break;
				
			case AGENCY_FARE_URL:
				p->agency->fare_url = estrdup(field);
				break;
				
			case AGENCY_EMAIL:
				p->agency->email = estrdup(field);
				break;
				
			default:
				fprintf(stderr, "Error creating AGENCY: Unknown field %d\n", p->cols_map[p->field]);
				break;
		}
	}
	
	p->field++;
}

static void cb2(int chr, agency_csv_parser_data_t *p)
{
	if (p->row_cntr > 0)
		hashtable_set
		(
			p->db->agency,
			p->agency->id,
			p->agency,
			HT_CMP_DATA(&agency_cmp),
			HT_PRINT_DATA(&agency_print),
			HT_FREE_DATA(&agency_free)
		);
	
	p->field = 0;
	p->row_cntr++;
	
	p->agency = ecalloc(1, sizeof *p->agency);
}

ssize_t agency_read_file(FILE *fp, gtfs_database_t *db)
{
	size_t bytes_read, retval, pos = 0;
	struct csv_parser cp;
	char buf[1024];
	
	printf("Reading AGENCY...\n");
	db->agency = hashtable_init(16);
	
	agency_csv_parser_data_t data;
	data.field = 0;
	data.row_cntr = 0;
	data.agency = NULL;
	data.db = db;
	memset(data.cols_map, 0, sizeof data.cols_map);
	
	csv_init(&cp, CSV_APPEND_NULL | CSV_EMPTY_IS_NULL);
	
	while ((bytes_read = fread(buf, 1, sizeof buf, fp)) > 0)
	{
		if ((retval = csv_parse(&cp, buf, bytes_read, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data)) != bytes_read)
		{
			if (csv_error(&cp) == CSV_EPARSE) 
			{
				printf("AGENCY malformed at byte %lu\n", (unsigned long) pos + retval + 1);
				data.row_cntr = -1;
				
				goto end;
			}
			else
			{
				printf("Error while processing AGENCY: %s\n", csv_strerror(csv_error(&cp)));
				data.row_cntr = -1;
				
				goto end;
			}
		}
		pos += bytes_read;
	}
	
end:
	csv_fini(&cp, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data);
	csv_free(&cp);

	efree(data.agency);

	return data.row_cntr;
}
