#ifndef _AGENCY_H_
#define _AGENCY_H_

#include <stdio.h>      // for FILE
#include <sys/types.h>  // for ssize_t

#include "types.h"      // for url_t, email_t, gtfs_database_t, gtfs_id_t

struct _agency_t
{
	gtfs_id_t id;
	text_t name;
	url_t url;
	timezone_t timezone;
	lang_code_t lang;
	phone_t phone;
	url_t fare_url;
	email_t email;
};

typedef enum
{
	AGENCY_ID = 1,
	AGENCY_NAME,
	AGENCY_URL,
	AGENCY_TIMEZONE,
	AGENCY_LANG,
	AGENCY_PHONE,
	AGENCY_FARE_URL,
	AGENCY_EMAIL,
	agency_fields_t_NUM_MEMBERS
} agency_fields_t;

extern const char *AGENCY_FIELDS[];

void agency_print_compact(const char *title, const agency_t *agency, const char *fallback);
void agency_print(const agency_t *agency);
int agency_cmp(agency_t * const *agency1, agency_t * const *agency2);
void agency_free(agency_t **agency);
ssize_t agency_read_file(FILE *fp, gtfs_database_t *db);

#endif //_AGENCY_H_
