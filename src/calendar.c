#include "calendar.h"

#include <assert.h>     // for assert
#include <csv.h>        // for csv_error, csv_fini, csv_free, csv_init, csv_...
#include <string.h>     // for memset
#include <time.h>       // for strftime, strptime
#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#endif

#include "hashtable.h"  // for hashtable_init, hashtable_set, HT_FREE_VALUE
#include "utils.h"      // for estrtol, ecalloc, efree, estrdup, get_index

#include "debug.h"

const char *CALENDAR_FIELDS[] = 
{
	"NULL",
	"service_id",
	"monday",
	"tuesday",
	"wednesday",
	"thursday",
	"friday",
	"saturday",
	"sunday",
	"start_date",
	"end_date"
};

typedef struct
{
	size_t field;
	ssize_t row_cntr;
	calendar_t *calendar;
	const gtfs_database_t *db;
	calendar_fields_t cols_map[calendar_fields_t_NUM_MEMBERS];
} calendar_csv_parser_data_t;

void calendar_print_compact(const char *title, const calendar_t *calendar, const char *fallback)
{
	assert(title != NULL);
	assert(fallback != NULL);
	
	calendar_dates_t **excs;
	size_t num_ele, added = 0, removed = 0;
	if (calendar == NULL)
		printf("%s: %s\n", title, fallback);
	else
	{
		num_ele = list_get_elements(&calendar->exceptions, &excs);
		for (size_t i = 0; i < num_ele; i++)
		{
			added += excs[i]->exception_type == ADDED_SERVICE;
			removed += excs[i]->exception_type == REMOVED_SERVICE;
		}
		efree(excs);
		
		printf("%s: |%s|%s|%s|%s|%s|%s|%s| %s --> ", title,
			calendar->monday == SERVICE_AVAILABLE ? "M" : " ",
			calendar->tuesday == SERVICE_AVAILABLE ? "T" : " ",
			calendar->wednesday == SERVICE_AVAILABLE ? "W" : " ",
			calendar->thursday == SERVICE_AVAILABLE ? "T" : " ",
			calendar->friday == SERVICE_AVAILABLE ? "F" : " ",
			calendar->saturday == SERVICE_AVAILABLE ? "S" : " ",
			calendar->sunday == SERVICE_AVAILABLE ? "S" : " ",
			date_time_t_to_str("%F", calendar->start_date));
		printf("%s |+%3zu|-%3zu| (%s)\n", 
			date_time_t_to_str("%F", calendar->end_date),
			added, removed, calendar->service_id);
	}
}

void calendar_print(const calendar_t *calendar)
{
	assert(calendar != NULL);
	
	const char add_rem[] = {'?', '+', '-'};
	size_t num_ele;
	calendar_dates_t **excs;
	
	#pragma omp critical(calendar_print)
	{
		printf("--------------------\n");
		printf("service_id: %s\n", calendar->service_id);
		printf("monday: %d\n", calendar->monday);
		printf("tuesday: %d\n", calendar->tuesday);
		printf("wednesday: %d\n", calendar->wednesday);
		printf("thursday: %d\n", calendar->thursday);
		printf("friday: %d\n", calendar->friday);
		printf("saturday: %d\n", calendar->saturday);
		printf("sunday: %d\n", calendar->sunday);
		printf("start_date: %s\n", date_time_t_to_str("%F", calendar->start_date));
		printf("end_date: %s\n", date_time_t_to_str("%F", calendar->end_date));
		
		num_ele = list_get_elements(&calendar->exceptions, &excs);
		for (size_t i = 0; i < num_ele; i++)
		{
			printf("exception[%zu]: %s: %c\n", 
				i, 
				date_time_t_to_str("%F", excs[i]->date),
				add_rem[excs[i]->exception_type]);
		}
		efree(excs);
		printf("--------------------\n");
	}
}

int calendar_cmp(calendar_t * const *cal1, calendar_t * const *cal2)
{
	if (*cal1 == NULL && *cal2 == NULL) return 0;
	if (*cal1 == NULL) return -1;
	if (*cal2 == NULL) return 1;
	
	if (*cal1 == *cal2) return 0;
	
	return strcmp((*cal1)->service_id, (*cal2)->service_id);
}

void calendar_free(calendar_t **calendar)
{
	calendar_t *cal;
	
	assert(calendar != NULL);
	assert(*calendar != NULL);
	
	cal = *calendar;
		
	efree(cal->service_id);
	list_free(&cal->exceptions);
	
	efree(cal);
	
	*calendar = NULL;
}

static void cb1(char *field, size_t size, calendar_csv_parser_data_t *p)
{
	ssize_t index;
	
	if (p->row_cntr == 0)
	{
		index = get_index(CALENDAR_FIELDS, field, calendar_fields_t_NUM_MEMBERS);
		if (index >= 0)
		{
			
			p->cols_map[p->field] = (calendar_fields_t) index;
		}
		else
			fprintf(stderr, "Error reading CALENDAR file: Unknown header <%s>\n", field);
	}
	else
	{
		switch (p->cols_map[p->field])
		{
			case CALENDAR_SERVICE_ID:
				p->calendar->service_id = estrdup(field);
				break;
				
			case CALENDAR_MONDAY:
				p->calendar->monday = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case CALENDAR_TUESDAY:
				p->calendar->tuesday = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case CALENDAR_WEDNESDAY:
				p->calendar->wednesday = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case CALENDAR_THURSDAY:
				p->calendar->thursday = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case CALENDAR_FRIDAY:
				p->calendar->friday = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case CALENDAR_SATURDAY:
				p->calendar->saturday = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case CALENDAR_SUNDAY:
				p->calendar->sunday = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case CALENDAR_START_DATE:
				p->calendar->start_date = str_to_date_time_t("%Y%m%d", field);
				break;
				
			case CALENDAR_END_DATE:
				p->calendar->end_date = str_to_date_time_t("%Y%m%d", field);
				break;
				
			default:
				fprintf(stderr, "Error creating CALENDAR: Unknown field %d\n", p->cols_map[p->field]);
				break;
		}
	}
	
	p->field++;
}

static void cb2(int chr, calendar_csv_parser_data_t *p)
{
	if (p->row_cntr > 0)
	{
		list_init(&p->calendar->exceptions, 1, efree_wrapper, NULL);
		
		hashtable_set
		(
			p->db->calendar,
			p->calendar->service_id,
			p->calendar,
			HT_CMP_DATA(&calendar_cmp),
			HT_PRINT_DATA(&calendar_print),
			HT_FREE_DATA(&calendar_free)
		);
	}
	
	p->field = 0;
	p->row_cntr++;
	
	p->calendar = ecalloc(1, sizeof *p->calendar);
}

void calendar_add_calendar_date(calendar_t *calendar, calendar_dates_t *calendar_date)
{
	list_add(&calendar->exceptions, calendar_date);
}

ssize_t calendar_read_file(FILE *fp, gtfs_database_t *db)
{
	size_t bytes_read, retval, pos = 0;
	struct csv_parser cp;
	char buf[1024];
	
	printf("Reading CALENDAR...\n");
	db->calendar = hashtable_init(256);
	
	calendar_csv_parser_data_t data;
	data.field = 0;
	data.row_cntr = 0;
	data.calendar = NULL;
	data.db = db;
	memset(data.cols_map, 0, sizeof data.cols_map);
	
	csv_init(&cp, CSV_APPEND_NULL | CSV_EMPTY_IS_NULL);
	
	while ((bytes_read = fread(buf, 1, sizeof buf, fp)) > 0)
	{
		if ((retval = csv_parse(&cp, buf, bytes_read, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data)) != bytes_read)
		{
			if (csv_error(&cp) == CSV_EPARSE) 
			{
				printf("CALENDAR malformed at byte %lu\n", (unsigned long) pos + retval + 1);
				data.row_cntr = -1;
				
				goto end;
			}
			else
			{
				printf("Error while processing CALENDAR: %s\n", csv_strerror(csv_error(&cp)));
				data.row_cntr = -1;
				
				goto end;
			}
		}
		pos += bytes_read;
	}
	
end:
	csv_fini(&cp, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data);
	csv_free(&cp);

	efree(data.calendar);

	return data.row_cntr;
}
