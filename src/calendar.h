#ifndef _CALENDAR_H_
#define _CALENDAR_H_

#include <stdio.h>      // for FILE
#include <sys/types.h>  // for ssize_t

#include "list.h"
#include "types.h"      // for date_time_t, gtfs_database_t, gtfs_id_t

typedef enum
{
	SERVICE_NOT_AVAILABLE = 0,
	SERVICE_AVAILABLE = 1
} calendar_service_availability_t;

typedef enum
{
	UNKNOWN_SERVICE = 0,
	ADDED_SERVICE = 1,
	REMOVED_SERVICE = 2
} calendar_dates_exception_t;

struct _calendar_dates_t
{
	date_time_t date;
	calendar_dates_exception_t exception_type;
};

struct _calendar_t
{
	gtfs_id_t service_id;
	calendar_service_availability_t monday;
	calendar_service_availability_t tuesday;
	calendar_service_availability_t wednesday;
	calendar_service_availability_t thursday;
	calendar_service_availability_t friday;
	calendar_service_availability_t saturday;
	calendar_service_availability_t sunday;
	date_time_t start_date;
	date_time_t end_date;
	
	list_t exceptions;
};

typedef enum
{
	CALENDAR_DATES_SERVICE_ID = 1,
	CALENDAR_DATES_DATE,
	CALENDAR_DATES_EXCEPTION_TYPE,
	calendar_dates_fields_t_NUM_MEMBERS
} calendar_dates_fields_t;

typedef enum
{
	CALENDAR_SERVICE_ID = 1,
	CALENDAR_MONDAY,
	CALENDAR_TUESDAY,
	CALENDAR_WEDNESDAY,
	CALENDAR_THURSDAY,
	CALENDAR_FRIDAY,
	CALENDAR_SATURDAY,
	CALENDAR_SUNDAY,
	CALENDAR_START_DATE,
	CALENDAR_END_DATE,
	calendar_fields_t_NUM_MEMBERS
} calendar_fields_t;

extern const char *CALENDAR_FIELDS[];
extern const char *CALENDAR_DATES_FIELDS[];

void calendar_print_compact(const char *title, const calendar_t *calendar, const char *fallback);
void calendar_print(const calendar_t *calendar);
int calendar_cmp(calendar_t * const *cal1, calendar_t * const *cal2);
void calendar_free(calendar_t **calendar);
void calendar_add_calendar_date(calendar_t *calendar, calendar_dates_t *calendar_date);
ssize_t calendar_read_file(FILE *fp, gtfs_database_t *db);
ssize_t calendar_dates_read_file(FILE *fp, gtfs_database_t *db);

#endif //_CALENDAR_H_
