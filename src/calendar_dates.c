#include "calendar.h"

#include <assert.h>     // for assert
#include <csv.h>        // for csv_error, csv_fini, csv_free, csv_init, csv_...
#include <string.h>     // for memset, strncmp
#include <time.h>       // for strftime, strptime
#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "hashtable.h"  // for hashtable_get, hashtable_init, hashtable_loopall
#include "utils.h"      // for ecalloc, efree, estrdup, estrtol, get_index

#include "debug.h"

#define TID omp_get_thread_num()
#define Thds omp_get_max_threads()

const char *CALENDAR_DATES_FIELDS[] = 
{
	"NULL",
	"service_id",
	"date",
	"exception_type"
};

typedef struct
{
	size_t *field;
	ssize_t row_cntr;
	gtfs_id_t *service_id;
	calendar_dates_t **calendar_date;
	const gtfs_database_t *db;
	calendar_dates_fields_t cols_map[calendar_dates_fields_t_NUM_MEMBERS];
} calendar_dates_csv_parser_data_t;

static void cb1(char *field, size_t size, calendar_dates_csv_parser_data_t *p)
{
	ssize_t index;
	
	if (p->row_cntr == 0)
	{
		index = get_index(CALENDAR_DATES_FIELDS, field, calendar_dates_fields_t_NUM_MEMBERS);
		if (index >= 0)
		{
			p->cols_map[p->field[TID]] = (calendar_dates_fields_t) index;
		}
		else
			fprintf(stderr, "Error reading CALENDAR_DATES file: Unknown header <%s>\n", field);
	}
	else
	{
		switch (p->cols_map[p->field[TID]])
		{
			case CALENDAR_DATES_SERVICE_ID:
				p->service_id[TID] = estrdup(field);
				break;
				
			case CALENDAR_DATES_DATE:
				p->calendar_date[TID]->date = str_to_date_time_t("%Y%m%d", field);
				break;
				
			case CALENDAR_DATES_EXCEPTION_TYPE:
				p->calendar_date[TID]->exception_type = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			default:
				fprintf(stderr, "Error creating CALENDAR_DATES: Unknown field %d\n", p->cols_map[p->field[TID]]);
				break;
		}
	}
	
	p->field[TID]++;
}

static void cb2(int chr, calendar_dates_csv_parser_data_t *p)
{
	calendar_t *service;
	value_t *val;
	
	if (p->row_cntr > 0)
	{
		val = hashtable_get(p->db->calendar, p->service_id[TID]);
		if (val == NULL)
		{
			service = ecalloc(1, sizeof *service);
			
			service->service_id = p->service_id[TID];
			service->monday = SERVICE_NOT_AVAILABLE;
			service->tuesday = SERVICE_NOT_AVAILABLE;
			service->wednesday = SERVICE_NOT_AVAILABLE;
			service->thursday = SERVICE_NOT_AVAILABLE;
			service->friday = SERVICE_NOT_AVAILABLE;
			service->saturday = SERVICE_NOT_AVAILABLE;
			service->sunday = SERVICE_NOT_AVAILABLE;
			service->start_date = str_to_date_time_t("%Y%m%d", "19900101");
			service->end_date = str_to_date_time_t("%Y%m%d", "21001231");
			
			list_init(&service->exceptions, 1, efree_wrapper, NULL);
			list_add(&service->exceptions, p->calendar_date[TID]);
			
			hashtable_set
			(
				p->db->calendar,
				p->service_id[TID],
				service,
				HT_CMP_DATA(&calendar_cmp),
				HT_PRINT_DATA(&calendar_print),
				HT_FREE_DATA(&calendar_free)
			);
		}
		else
		{
			service = val->data;
			calendar_add_calendar_date(service, p->calendar_date[TID]);
			
			efree(p->service_id[TID]);
		}
	}
	else
	{
		for (int i = 1; i < omp_get_num_threads(); i++)
		{
			p->calendar_date[i] = ecalloc(1, sizeof *p->calendar_date[i]);
		}
	}
	
	p->field[TID] = 0;
	
	#pragma omp atomic update
	p->row_cntr++;
	
	p->calendar_date[TID] = ecalloc(1, sizeof *p->calendar_date[TID]);
}

ssize_t calendar_dates_read_file(FILE *fp, gtfs_database_t *db)
{
	size_t bytes_read, retval, pos = 0;
	struct csv_parser cp;
	char *res;
	char buf[1024];
	
	printf("Reading CALENDAR_DATES...\n");
	if (db->calendar == NULL)
		db->calendar = hashtable_init(2048);
	
	calendar_dates_csv_parser_data_t data;
	data.field = ecalloc((size_t) Thds, sizeof *data.field);
	data.row_cntr = 0;
	data.service_id = ecalloc((size_t) Thds, sizeof *data.service_id);
	data.calendar_date = ecalloc((size_t) Thds, sizeof *data.calendar_date);
	data.db = db;
	memset(data.cols_map, 0, sizeof data.cols_map);
	
	#pragma omp parallel private(buf, cp, res, bytes_read, retval)
	{
	bytes_read = 0;
	csv_init(&cp, CSV_APPEND_NULL | CSV_EMPTY_IS_NULL);
	
	while (1)
	{
		while (TID != 0 && data.row_cntr == 0)
		;
		
		#pragma omp critical(calendar_dates_read_line)
		{
			pos += bytes_read;
			res = fgets(buf, sizeof buf, fp);
		}
		if (res == NULL) break;
		bytes_read = strlen(buf);
		
		if ((retval = csv_parse(&cp, buf, bytes_read, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data)) != bytes_read)
		{
			if (csv_error(&cp) == CSV_EPARSE) 
			{
				printf("CALENDAR_DATES malformed at byte %lu\n", (unsigned long) pos + retval + 1);
				
				break;
			}
			else
			{
				printf("Error while processing CALENDAR_DATES: %s\n", csv_strerror(csv_error(&cp)));
				
				break;
			}
		}
	}
	
	csv_fini(&cp, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data);
	csv_free(&cp);
	
	efree(data.calendar_date[TID]);
	}
	
	efree(data.service_id);
	efree(data.calendar_date);
	efree(data.field);
	
	return data.row_cntr;
}
