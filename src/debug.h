#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include "./term_colors.h"

#define DEB_VERB 	4
#define DEB_INFO 	3
#define DEB_WARN 	2
#define DEB_ERR 	1

#ifdef DEBUG
#define DPRINT_VERB(...) \
if (DEBUG >= DEB_VERB) { \
	printf(BOLD "[V %s:%d] " RESET, __FILE__, __LINE__); \
	printf(__VA_ARGS__); \
	printf(RESET); }

#define DPRINT_INFO(...) \
if (DEBUG >= DEB_INFO) { \
	printf(CYN BOLD "[I %s:%d] " RESET CYN, __FILE__, __LINE__); \
	printf(__VA_ARGS__); \
	printf(RESET); }

#define DPRINT_WARN(...) \
if (DEBUG >= DEB_WARN) { \
	printf(YEL BOLD "[W %s:%d] " RESET YEL, __FILE__, __LINE__); \
	printf(__VA_ARGS__); \
	printf(RESET); }

#define DPRINT_ERRO(...) \
if (DEBUG >= DEB_ERR) { \
	fprintf(stderr, RED BOLD "[E %s:%d] " RESET RED, __FILE__, __LINE__); \
	fprintf(stderr, __VA_ARGS__); \
	fprintf(stderr, RESET); }
	
#else
#define DPRINT_VERB(...)
#define DPRINT_INFO(...)
#define DPRINT_WARN(...)
#define DPRINT_ERRO(...)
#endif

#endif
