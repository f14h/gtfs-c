#include "gtfs.h"

#include <assert.h>     // for assert
#include <stdio.h>      // for NULL, fclose, fopen, fprintf, perror, FILE
#include <string.h>     // for strlen, strncmp, strcat, strndup
#include <unistd.h>     // for access, ssize_t, F_OK

#include "agency.h"     // for agency_read_file
#include "stops.h"      // for stops_read_file
#include "routes.h"     // for routes_read_file
#include "trips.h"      // for trips_read_file
#include "stop_times.h" // for stop_times_read_file
#include "calendar.h"   // for calendar_read_file
#include "transfers.h"  // for transfers_read_file

#include "hashtable.h"  // for hashtable_t

#include "utils.h"      // for erealloc, efree

const char *GTFS_DATASET_FILES[] = 
{
	"agency.txt",
	"stops.txt",
	"routes.txt",
	"trips.txt",
	"stop_times.txt",
	"calendar.txt",
	"calendar_dates.txt",
	"fare_attributes.txt",
	"fare_rules.txt",
	"shapes.txt",
	"frequencies.txt",
	"transfers.txt",
	"pathways.txt",
	"levels.txt",
	"feed_info.txt",
	"translations.txt",
	"attributions.txt"
};

gtfs_database_t db = {0};

ssize_t gtfs_parse_file(gtfs_dataset_t dataset_type, const char *fldr)
{
	FILE *fp;
	ssize_t ret = -2;
	char *filename;
		
	assert(dataset_type < gtfs_dataset_t_NUM_MEMBERS);
	assert(dataset_type >= 0);
	assert(fldr != NULL);
	assert(strncmp(fldr, "", 5) != 0);
	
	filename = strndup(fldr, 4095);
	filename = erealloc(filename, strlen(filename) + strlen(GTFS_DATASET_FILES[dataset_type]) + 2, sizeof *filename);
	strcat(filename, "/");
	strcat(filename, GTFS_DATASET_FILES[dataset_type]);
	
	if(access(filename, F_OK) == -1)
	{
		perror(filename);

		efree(filename);
		return -1;
	}
	
	fp = fopen(filename, "r");
	switch (dataset_type)
	{
		case AGENCY: 
			ret = agency_read_file(fp, &db); 
			break;
			
		case STOPS:
			ret = stops_read_file(fp, &db);
			break;
			
		case ROUTES:
			ret = routes_read_file(fp, &db); 
			break;
			
		case TRIPS:
			ret = trips_read_file(fp, &db); 
			break;
			
		case STOP_TIMES:
			ret = stop_times_read_file(fp, &db); 
			break;
			
		case CALENDAR:
			ret = calendar_read_file(fp, &db); 
			break;
			
		case CALENDAR_DATES:
			ret = calendar_dates_read_file(fp, &db); 
			break;
		
		case FARE_ATTRIBUTES:
			//ret = fare_attributes_read_file(fp, &db); 
			break;
			
		case FARE_RULES:
			//ret = fare_rules_read_file(fp, &db); 
			break;
			
		case SHAPES:
			//ret = shapes_read_file(fp, &db); 
			break;
			
		case FREQUENCIES:
			//ret = frequencies_read_file(fp, &db); 
			break;
		
		case TRANSFERS:
			ret = transfers_read_file(fp, &db); 
			break;
			
		case PATHWAYS:
			//ret = pathways_read_file(fp, &db); 
			break;
			
		case LEVELS:
			//ret = levels_read_file(fp, &db); 
			break;
			
		case FEED_INFO:
			//ret = feed_info_read_file(fp, &db); 
			break;
			
		case TRANSLATIONS:
			//ret = translations_read_file(fp, &db); 
			break;
			
		case ATTRIBUTIONS:
			//ret = atributions_read_file(fp, &db); 
			break;
			
		default:
			fprintf(stderr, "Error reading GTFS file: Unknown file %s (%d)\n", filename, dataset_type);
			ret = -1;
			break;
	}
	fclose(fp);
	efree(filename);
	
	return ret;
}

void gtfs_free_db(void)
{
	hashtable_free(&db.agency);
	hashtable_free(&db.stops);
	hashtable_free(&db.routes);
	hashtable_free(&db.trips);
	hashtable_free(&db.stop_times);
	hashtable_free(&db.calendar);
	hashtable_free(&db.fare_attributes);
	hashtable_free(&db.fare_rules);
	hashtable_free(&db.shapes);
	hashtable_free(&db.frequencies);
	hashtable_free(&db.transfers);
	hashtable_free(&db.pathways);
	hashtable_free(&db.levels);
	hashtable_free(&db.feed_info);
	hashtable_free(&db.translations);
	hashtable_free(&db.attributions);
}
