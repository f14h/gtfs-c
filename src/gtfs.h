#ifndef _GTFS_H_
#define _GTFS_H_

#include <sys/types.h>  // for ssize_t

#include "types.h"      // for gtfs_database_t

typedef enum _gtfs_dataset_t
{
	AGENCY = 0,
	STOPS,
	ROUTES,
	TRIPS,
	STOP_TIMES,
	CALENDAR,
	CALENDAR_DATES,
	FARE_ATTRIBUTES,
	FARE_RULES,
	SHAPES,
	FREQUENCIES,
	TRANSFERS,
	PATHWAYS,
	LEVELS,
	FEED_INFO,
	TRANSLATIONS,
	ATTRIBUTIONS,
	gtfs_dataset_t_NUM_MEMBERS
} gtfs_dataset_t;

extern const char *GTFS_DATASET_FILES[];
extern gtfs_database_t db;

ssize_t gtfs_parse_file(gtfs_dataset_t dataset_type, const char *fldr);
void gtfs_free_db(void);

#endif //_GTFS_H_
