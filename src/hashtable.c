#include "hashtable.h"

#include <assert.h>  // for assert
#include <string.h>  // for strcmp, strlen
#include <stdint.h>  // for uint64_t

#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "utils.h"   // for efree, emalloc, ecalloc, estrdup

// DJB2 (http://www.partow.net/programming/hashfunctions/#DJBHashFunction)
__attribute__((no_sanitize("integer")))
uint64_t hash(const char *str, size_t length)
{
  	uint64_t hash = 5381;

  	for (size_t i = 0; i < length; ++str, ++i)
  	{
  		hash = ((hash << 5) + hash) + (uint64_t) (*str);
   	}

  	return hash;
}

hashtable_t *hashtable_init(size_t capacity)
{
	hashtable_t *htable;
	
	assert(capacity > 0);
		
	htable = emalloc(1, sizeof *htable);
	htable->capacity = capacity;
	htable->table = ecalloc(capacity, sizeof *htable->table);
	
	return htable;
}

void hashtable_free(hashtable_t **htbl)
{
	assert(htbl != NULL);
	
	if (htbl == NULL || *htbl == NULL) return;
	
	hashtable_t *htable = *htbl;
	
	#pragma omp parallel for simd
	for (size_t i = 0; i < htable->capacity; i++)
	{
		node_t *next = htable->table[i];
		node_t *old;
		while (next != NULL)
		{
			old = next;
			next = next->next;
			
			efree(old->key);
			old->value.free_data(&old->value.data);
			efree(old);
		}
	}
	
	efree(htable->table);
	efree(htable);
	
	*htbl = NULL;
}

void hashtable_set
(
	hashtable_t *htable,
	const char *key,
	void *data,
	int (*sort_data)(const void *, const void *),
	void (*print_data)(const void *),
	void (*free_data)(void **)
)
{
	assert(htable != NULL);
	assert(htable->capacity > 0);
	assert(key != NULL);
	assert(data != NULL);
	assert(sort_data != NULL);
	assert(print_data != NULL);
	assert(free_data != NULL);
	if (htable == NULL) return;
	
	uint64_t hsh = hash(key, strlen(key));
	size_t pos = hsh % htable->capacity;
	
	node_t *node = emalloc(1, sizeof *node);
	node->key = estrdup(key);
	node->value.data = data;
	node->value.sort_data = sort_data;
	node->value.print_data = print_data;
	node->value.free_data = free_data;
	
	//#pragma omp critical(hashtable_set)
	#pragma omp atomic capture
	{
		node->next = htable->table[pos];
	
		htable->table[pos] = node;
	}
}

value_t *hashtable_get(const hashtable_t *htable, const char *key)
{
	if (htable == NULL || key == NULL) return NULL;
	
	uint64_t hsh = hash(key, strlen(key));
	size_t pos = hsh % htable->capacity;
	
	node_t *next = htable->table[pos];
	while (next != NULL)
	{
		if (strcmp(next->key, key) == 0)
			return &next->value;

		next = next->next;
	}
	
	return NULL;
}

void *hashtable_get_data(const hashtable_t *htable, const char *key)
{
	value_t *val = hashtable_get(htable, key);
	if (val != NULL) return val->data;
	else return NULL;
}

void hashtable_loopall(const hashtable_t *htable, void (*cb)(void *, const void *), const void *p)
{
	if (htable == NULL) return;
	
	#pragma omp parallel for simd
	for (size_t i = 0; i < htable->capacity; i++)
	{
		node_t *next = htable->table[i];
		while (next != NULL)
		{
			cb(next->value.data, p);
			
			next = next->next;
		}
	}
}
