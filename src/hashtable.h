#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_

#include <stddef.h>

#define HT_CMP_DATA(fkt) ((int (*)(const void *, const void *)) (fkt))
#define HT_PRINT_DATA(fkt) ((void (*)(const void *)) (fkt))
#define HT_FREE_DATA(fkt) ((void (*)(void **)) (fkt))
#define HT_CB_LOOP(fkt) ((void (*)(void *, const void *)) (fkt))

typedef struct
{
	void *data;
	
	int (*sort_data)(const void *, const void *);
	void (*print_data)(const void *);
	void (*free_data)(void **);
} value_t;

typedef struct _node_t
{
	char *key;
	value_t value;
	
	struct _node_t *next;
} node_t;

typedef struct 
{
	size_t capacity;
	node_t **table;
} hashtable_t;

hashtable_t *hashtable_init(size_t capacity);
void hashtable_free(hashtable_t **htable);

void hashtable_set(hashtable_t *htable, const char *key, void *data, int (*sort_data)(const void *, const void *), void (*print_data)(const void *), void (*free_data)(void **));
value_t *hashtable_get(const hashtable_t *htable, const char *key);
void *hashtable_get_data(const hashtable_t *htable, const char *key);
void hashtable_loopall(const hashtable_t *htable, void (*cb)(void *, const void *), const void *p);

#endif //_HASHTABLE_H_

