#include "list.h"

#include <assert.h>
#include <string.h>
#include <stdint.h>

#ifdef _OPENMP
	#include <omp.h>
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "hashtable.h"
#include "utils.h"

list_t *list_create(size_t list_size, void (*free_data)(void **), int (*sort_data)(const void *, const void *))
{
	list_t *list = emalloc(1, sizeof *list);
	list->size = MAX(list_size, 0U);
	list->num_elements = 0;
	list->free_data = free_data;
	list->sort_data = sort_data;
	list->base = emalloc(list_size, sizeof *list->base);
	
	return list;
}

void list_init(list_t *list, size_t list_size, void (*free_data)(void **), int (*sort_data)(const void *, const void *))
{
	list->size = MAX(list_size, 0U);
	list->num_elements = 0;
	list->free_data = free_data;
	list->sort_data = sort_data;
	list->base = emalloc(list_size, sizeof *list->base);
}

void list_free(list_t *list)
{
	assert(list != NULL);
	
	if (list->free_data != NULL)
		for (size_t i = 0; i < list->num_elements; i++)
			list->free_data(&list->base[i]);
	
	efree(list->base);
}

void list_add(list_t *list, void *element)
{
	assert(list != NULL);
	assert(element != NULL);
	
	if (list->num_elements == list->size)
	{
		list->size *= 2;
		list->base = erealloc(list->base, list->size, sizeof *list->base);
	}
	
	list->base[list->num_elements] = element;
	list->num_elements++;
	
	if (list->sort_data != NULL)
		qsort(list->base, list->num_elements, sizeof *list->base, HT_CMP_DATA(list->sort_data));
}

void *list_search(list_t *list, void *element)
{
	assert(list != NULL);
	assert(element != NULL);
	assert(list->sort_data != NULL);
	
	void **ele = bsearch(&element,
			list->base,
			list->num_elements,
			sizeof *list->base,
			HT_CMP_DATA(list->sort_data));
	
	if (ele == NULL) return NULL;
	else return *ele;
}

void *list_pop_last(list_t *list)
{
	assert(list != NULL);
	
	if (list->num_elements == 0) return NULL;
	
	void *ele = list->base[list->num_elements - 1U];
	list->num_elements--;
	
	return ele;
}

size_t list_get_elements(const list_t *list, void *elements)
{
	assert(list != NULL);
	assert(elements != NULL);
	
	void **eles = elements;
	
	*eles = emalloc(list->num_elements, sizeof *eles);
	memcpy(*eles, list->base, list->num_elements * sizeof *eles);
	
	return list->num_elements;
}
