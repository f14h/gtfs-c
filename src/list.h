#ifndef _LIST_H_
#define _LIST_H_

#include <stddef.h>

typedef struct _list_t
{
	size_t size;
	size_t num_elements;
	void (*free_data)(void **);
	int (*sort_data)(const void *, const void *);
	
	void **base;
} list_t;

list_t *list_create(size_t list_size, void (*free_data)(void **), int (*sort_data)(const void *, const void *));
void list_init(list_t *list, size_t list_size, void (*free_data)(void **), int (*sort_data)(const void *, const void *));
void list_free(list_t *list);
void list_add(list_t *list, void *element);
void *list_search(list_t *list, void *element);
void *list_pop_last(list_t *list);
size_t list_get_elements(const list_t *list, void *elements);

#endif //_LIST_H_
