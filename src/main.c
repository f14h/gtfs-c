#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <sys/time.h>

#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
	#define omp_set_nested(n)
	#define omp_set_num_threads(n)
#endif

#include "gtfs.h"		// for gtfs_parse_file, AGENCY
#include "queries.h"

#include "utils.h"

#include "agency.h"
#include "stops.h"
#include "routes.h"
#include "trips.h"
#include "stop_times.h"
#include "calendar.h"
#include "transfers.h"

size_t main_parse_cmd(const char *cmd);

int main(int args, char *argv[])
{
	char *line = NULL;
	size_t len, results;
	ssize_t ret;
	bool end = false;
	struct timeval start, stop, res;
	
	tzset();
	
	omp_set_num_threads(1);
	omp_set_nested(1);
	#pragma omp parallel sections
	{
		#pragma omp section
		{
			gtfs_parse_file(AGENCY, argv[1]);
			gtfs_parse_file(ROUTES, argv[1]);
			gtfs_parse_file(FARE_ATTRIBUTES, argv[1]);
		}
		#pragma omp section
		{
			gtfs_parse_file(CALENDAR, argv[1]);
			gtfs_parse_file(CALENDAR_DATES, argv[1]);
		}
		#pragma omp section
		{
			gtfs_parse_file(LEVELS, argv[1]);
			gtfs_parse_file(STOPS, argv[1]);
			gtfs_parse_file(FARE_RULES, argv[1]);
		}
		#pragma omp section
		{
			gtfs_parse_file(SHAPES, argv[1]);
		}
		#pragma omp section
		{
			gtfs_parse_file(FEED_INFO, argv[1]);
		}
	}
	#pragma omp parallel sections
	{
		#pragma omp section
		{
			gtfs_parse_file(TRIPS, argv[1]);
			gtfs_parse_file(FREQUENCIES, argv[1]);
		}
		#pragma omp section
		{
			gtfs_parse_file(FARE_RULES, argv[1]);
		}
	}
	#pragma omp parallel sections
	{
		#pragma omp section
		{
			gtfs_parse_file(STOP_TIMES, argv[1]);
			gtfs_parse_file(TRANSLATIONS, argv[1]);
		}
		#pragma omp section
		{
			gtfs_parse_file(TRANSFERS, argv[1]);
			gtfs_parse_file(ATTRIBUTIONS, argv[1]);
		}
	}
	omp_set_nested(0);
	
	do
	{
		printf("GTFS-DB > ");
		ret = getline(&line, &len, stdin);
		if (ret == -1 || strncmp(line, "q", 1) == 0 || strncmp(line, "quit", 4) == 0)
		{
			printf("Shutting down...\n");
			end = true;
		}
		else
		{
			gettimeofday(&start, NULL);
			results = main_parse_cmd(line);
			gettimeofday(&stop, NULL);
			
			timersub(&stop, &start, &res);
			printf("\nResults: %zu ~~ %.3f ms\n", results, res.tv_sec * 1000.0f + res.tv_usec / 1000.0f);
		}
		
		efree(line);
	} while (!end);
	
	gtfs_free_db();
	
	return 0;
}

size_t main_lookup(const char *cmd)
{
	char db_name[100], id[100];
	char *db_file;
	const value_t *value;
	size_t ret = 0;
	
	sscanf(cmd, "lookup %s %s", db_name, id);
	
	for (unsigned int dataset = 0; dataset < gtfs_dataset_t_NUM_MEMBERS; dataset++)
	{
		db_file = strdup(GTFS_DATASET_FILES[dataset]);
		if (strcmp(strtok(db_file, "."), db_name) == 0)
		{
			value = query_lookup_by_id(dataset, id);
			if (value != NULL)
			{
				value->print_data(value->data);
				ret = 1;
			}
			else
				fprintf(stderr, "No results found in %s for %s!\n", db_file, id);
		}
		efree(db_file);
	}
	
	return ret;
}

size_t main_leave(const char *cmd)
{
	char time_str[32], name[128];
	stops_t **stops;
	stop_times_t **stop_times;
	size_t res_stops, res_stop_times, res_tot = 0;
	date_time_t time;
	memset(&time, 0, sizeof time);
		
	sscanf(cmd, "leave %s %[^\n]", time_str, name);
	time = str_to_date_time_t("%d.%m.%Y_%H:%M", time_str);
	
	res_stops = query_get_stops_with_name(&stops, name);
	for (size_t i = 0; i < res_stops; i++)	// TODO menu selector for user
	{
		printf("Trips from %s:\n", stops[i]->name);
		res_stop_times = query_get_trips_at_station_with_departure(&stop_times, stops[i], time, 10, true);
		for (size_t j = 0; j < res_stop_times; j++)
		{
			stop_times_print(stop_times[j]);
		}
		efree(stop_times);
		
		res_tot += res_stop_times;
	}
	efree(stops);
	
	return res_tot;
}

size_t main_route(const char *cmd)
{
	char time_str[32], name[128], name2[128];
	stops_t **stops;
	stop_times_t **stop_times;
	stops_t *start, *dest;
	char *r;
	size_t res_stop_times;
	date_time_t time;
	memset(&time, 0, sizeof time);
		
	sscanf(cmd, "route %s %s %[^\n]", time_str, name, name2);
	time = str_to_date_time_t("%d.%m.%Y_%H:%M", time_str);
	while ((r = strchr(name, '_')) != NULL)
		*r = ' ';
	
	query_get_stops_with_name(&stops, name);
	start = stops[0];
	efree(stops);
	query_get_stops_with_name(&stops, name2);
	dest = stops[0];
	efree(stops);
	
	printf("Finding route %s --> %s:\n", start->name, dest->name);
	res_stop_times = query_get_route(&stop_times, start, dest, time, 20, 1);
	for (size_t i = res_stop_times; i > 0; i--)
	{
		if (stop_times[i-1] != NULL)
			stop_times_print(stop_times[i - 1]);
	}
	efree(stop_times);
	
	return res_stop_times;
}

size_t main_connecting(const char *cmd)
{
	char time_str[32], name[128], came_with_str[64];
	stops_t **stops;
	stop_times_t **stop_times;
	const stop_times_t *came_with;
	size_t res_stops, res_stop_times, res_tot = 0;
	date_time_t time;
	memset(&time, 0, sizeof time);
		
	sscanf(cmd, "connecting %s %s %[^\n]", time_str, came_with_str, name);
	time = str_to_date_time_t("%d.%m.%Y_%H:%M", time_str);
	came_with = query_lookup_data_by_id(STOP_TIMES, came_with_str);
	
	res_stops = query_get_stops_with_name(&stops, name);
	for (size_t i = 0; i < res_stops; i++)	// TODO menu selector for user
	{
		res_stop_times = query_get_connecting_trips(&stop_times, stops[i], came_with, time, 30);
		for (size_t j = 0; j < res_stop_times; j++)
		{
			stop_times_print(stop_times[j]);
		}
		efree(stop_times);
		
		res_tot += res_stop_times;
	}
	efree(stops);
	
	return res_tot;
}

size_t main_loc(const char *cmd)
{
	stops_t **stops;
	size_t res;
	float lat, lon;
	
	sscanf(cmd, "loc %f %f", &lat, &lon);
	
	res = query_get_stops_in_vicinity(&stops, lat, lon, 1.0);
	for (size_t i = 0; i < res; i++)
	{
		stops_print(stops[i]);
	}
	efree(stops);
	
	return res;
}

size_t main_search(const char *cmd)
{
	char name[128];
	stops_t **stops;
	size_t res;
	
	sscanf(cmd, "search %[^\n]", name);
	
	res = query_get_stops_with_name(&stops, name);
	for (size_t i = 0; i < res; i++)
	{
		stops_print(stops[i]);
	}
	efree(stops);
	
	return res;
}

size_t main_stops(const char *cmd)
{
	char time_str[32], line[16], station[32];
	date_time_t time;
	stops_t **stops;
	stop_times_t **stop_times;
	trips_t *trip;
	size_t res_stops, res_stop_times, res_tot = 0;
	char *r;
	
	sscanf(cmd, "stops %31s %15s %31[^\n]", time_str, line, station);
	time = str_to_date_time_t("%d.%m.%Y_%H:%M", time_str);
	while ((r = strchr(line, '_')) != NULL)
		*r = ' ';
	
	res_stops = query_get_stops_with_name(&stops, station);
	for (size_t i = 0; i < res_stops; i++)	// TODO menu selector for user
	{
		printf("Trips from %s:\n", stops[i]->name);
		
		res_stop_times = query_get_trips_at_station_with_departure(&stop_times, stops[i], time, 10, true);
		for (size_t j = 0; j < res_stop_times; j++)
		{
			trip = stop_times[j]->trip;
			trips_print_compact("Found trip", trip, "???");
			if (strcmp(trip->route->short_name != NULL ? trip->route->short_name : trip->route->long_name, line) != 0) continue;
			
			for (size_t k = 0; k < trip->num_stop_times; k++)
			{
				stop_times_print(trip->stop_times[k]);
			}
			res_tot += trip->num_stop_times;
		}
		efree(stop_times);
	}
	efree(stops);
	
	return res_tot;
}

size_t main_parse_cmd(const char *cmd)
{
	if (strncmp(cmd, "lookup", strlen("lookup")) == 0) return main_lookup(cmd);
	else if (strncmp(cmd, "leave", strlen("leave")) == 0) return main_leave(cmd);
	else if (strncmp(cmd, "loc", strlen("loc")) == 0) return main_loc(cmd);
	else if (strncmp(cmd, "search", strlen("search")) == 0) return main_search(cmd);
	else if (strncmp(cmd, "stops", strlen("stops")) == 0) return main_stops(cmd);
	else if (strncmp(cmd, "connecting", strlen("connecting")) == 0) return main_connecting(cmd);
	else if (strncmp(cmd, "route", strlen("route")) == 0) return main_route(cmd);
	else printf("Unknown command!\n");
	
	return 0;
}
