#include "queries.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>

#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "gtfs.h"
#include "utils.h"

////////////////////////////////

hashtable_t *query_get_database(gtfs_dataset_t dataset)
{
	switch (dataset)
	{
		case AGENCY:
			return db.agency;
		
		case STOPS:
			return db.stops;
		
		case ROUTES:
			return db.routes;
		
		case TRIPS:
			return db.trips;
			
		case STOP_TIMES:
			return db.stop_times;
			
		case CALENDAR:
			return db.calendar;
			
		case CALENDAR_DATES:
			return db.calendar;
			
		case FARE_ATTRIBUTES:
			return db.fare_attributes;
			
		case FARE_RULES:
			return db.fare_rules;
			
		case SHAPES:
			return db.shapes;
			
		case FREQUENCIES:
			return db.frequencies;
			
		case TRANSFERS:
			return db.transfers;
			
		case PATHWAYS:
			return db.pathways;
			
		case LEVELS:
			return db.levels;
			
		case FEED_INFO:
			return db.feed_info;
			
		case TRANSLATIONS:
			return db.translations;
			
		case ATTRIBUTIONS:
			return db.attributions;
		
		default:
			fprintf(stderr, "Unknown database (%d)!\n", dataset);
			return NULL;
	}
}

////////////////////////////////

const value_t *query_lookup_by_id(gtfs_dataset_t dataset, const char *key)
{
	return hashtable_get(query_get_database(dataset), key);
}

const void *query_lookup_data_by_id(gtfs_dataset_t dataset, const char *key)
{
	return hashtable_get_data(query_get_database(dataset), key);
}


////////////////////////////////

struct query_get_stops_with_name_t
{
	const char *name;
	size_t num_stops;
	stops_t **stops;
};

static void search_stop_name(stops_t *stop, struct query_get_stops_with_name_t *data)
{
	if (stop->parent_station == NULL && strcasestr(stop->name, data->name) != NULL)
	{
		#pragma omp critical(queries_search_stop_name)
		{
			data->stops = erealloc(data->stops, (data->num_stops + 1), sizeof *data->stops);
			data->stops[data->num_stops++] = stop;
		}
	}
}

size_t query_get_stops_with_name(stops_t *(*stops[]), const char *name)
{
	struct query_get_stops_with_name_t data;
	memset(&data, 0, sizeof data);
	data.name = name;
	
	hashtable_loopall(db.stops, HT_CB_LOOP(&search_stop_name), &data);
	
	qsort(data.stops, data.num_stops, sizeof *data.stops, HT_CMP_DATA(&stops_cmp));
	*stops = data.stops;
	return data.num_stops;
}

////////////////////////////////

struct query_get_stops_in_vicinity_t
{
	float lat, lon;
	float dist;
	size_t num_stops;
	stops_t **stops;
};

static void search_stop_loc(stops_t *stop, struct query_get_stops_in_vicinity_t *data)
{
	if (stop->parent_station != NULL) return;
	
	double lat1 = stop->lat * M_PI / 180.0;
	double lat2 = data->lat * M_PI / 180.0;
	double lon1 = stop->lon * M_PI / 180.0;
	double lon2 = data->lon * M_PI / 180.0;
	double dlon = lon2 - lon1;
	double dlat = lat2 - lat1;
	double a = pow(sin(dlat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlon / 2), 2);
	double d = 2 * 6373.0 * atan2(sqrt(a), sqrt(1 - a));
	
	if (d <= data->dist)
	{
		#pragma omp critical(queries_search_stop_loc)
		{
			data->stops = erealloc(data->stops, (data->num_stops + 1), sizeof *data->stops);
			data->stops[data->num_stops++] = stop;
		}
	}
}

size_t query_get_stops_in_vicinity(stops_t *(*stops[]), float lat, float lon, float dist)
{
	struct query_get_stops_in_vicinity_t data;
	memset(&data, 0, sizeof data);
	data.lat = lat;
	data.lon = lon;
	data.dist = dist;
	
	hashtable_loopall(db.stops, HT_CB_LOOP(&search_stop_loc), &data);
	
	qsort(data.stops, data.num_stops, sizeof *data.stops, HT_CMP_DATA(&stops_cmp));
	*stops = data.stops;
	return data.num_stops;
}

////////////////////////////////

typedef struct _route_nodes
{
	stops_t *stop;
	union
	{
		uint32_t cost_so_far;	// for visited nodes
		uint32_t priority;		// for frontier nodes
	};
	struct _route_nodes *came_from;
	stop_times_t *came_with;
} route_nodes_t;

static int route_nodes_cmp_by_dist(route_nodes_t * const *node1, route_nodes_t * const *node2)
{
	return date_time_t_cmp((*node2)->priority, (*node1)->priority);
}

static int route_nodes_cmp_by_stop(route_nodes_t * const *node1, route_nodes_t * const *node2)
{
	return stops_cmp(&(*node1)->stop, &(*node2)->stop);
}

static uint32_t route_estimate_cost(const stops_t *stop, const stops_t *dest)
{
	double dist;
	
	dist = query_get_distance_between_stops(stop, dest);
	
	// Convert to seconds, assume travel speed of 15m/s
	return (uint32_t) (dist * 1000.0 / 15.0);
}

static int32_t route_changing_cost
(
	const stops_t *at,
	const stop_times_t *from,
	const stop_times_t *to,
	uint32_t default_cost
)
{
	stops_t *stop_from;
	stops_t *stop_to;
	trips_t *trip_from, *trip_to;
	routes_t *route_from, *route_to;
	transfers_t *transfer;
	int32_t cost;
	bool cost_overwritten;
	double dist;
	
	if (from == NULL)
	{
		dist = query_get_distance_between_stops(at, to->stop);
		return (int32_t) (dist * 655); // 5.5 km/h in s/km, average walking speed
	}
	
	stop_from = from->stop;
	stop_to = to->stop;
	trip_from = from->trip;
	trip_to = to->trip;
	route_from = from->trip->route;
	route_to = to->trip->route;
	cost_overwritten = false;
	
	if (trip_to == trip_from) return 0;
	
	cost = 0;
	for (size_t i = 0; i < stop_from->num_transfers; i++)
	{
		transfer = stop_from->transfers[i];
	
		if (transfer->to_stop == stop_to)
		{
			if (!cost_overwritten)
			{
				switch (transfer->type)
				{
					// Vehicles wait for each other, no additional changing condition
					case TRANSFERS_TIMED:
						cost = 0U;
						break;
					
					// Passengers need at least a certain amount of time
					case TRANSFERS_TIME_NEEDED:
						cost = (int32_t) transfer->min_transfer_time;
						break;
					
					// Transfer is not possible here
					case TRANSFERS_NOT_POSSIBLE:
						cost = -1;
						break;
						
					// Use default cost otherwise
					default:
						cost = (int32_t) default_cost * 60; // to sec
						break;
				}
			}
			
			if (transfer->from_trip == trip_from &&
				transfer->to_trip == trip_to)
			{
				switch (transfer->type)
				{
					// Vehicles wait for each other, no additional changing condition
					case TRANSFERS_TIMED:
						cost = 0U;
						break;
					
					// Passengers need at least a certain amount of time
					case TRANSFERS_TIME_NEEDED:
						cost = (int32_t) transfer->min_transfer_time;
						break;
					
					// Transfer is not possible here
					case TRANSFERS_NOT_POSSIBLE:
						cost = -1;
						break;
						
					// Use default cost otherwise
					default:
						cost = (int32_t) default_cost * 60; // to sec
						break;
				}
				
				cost_overwritten = true;
			}
			if (transfer->from_route == route_from &&
				transfer->to_route == route_to)
			{
				switch (transfer->type)
				{
					// Vehicles wait for each other, no additional changing condition
					case TRANSFERS_TIMED:
						cost = 0U;
						break;
					
					// Passengers need at least a certain amount of time
					case TRANSFERS_TIME_NEEDED:
						cost = (int32_t) transfer->min_transfer_time;
						break;
					
					// Transfer is not possible here
					case TRANSFERS_NOT_POSSIBLE:
						cost = -1;
						break;
						
					// Use default cost otherwise
					default:
						cost = (int32_t) default_cost * 60; // to sec
						break;
				}
				
				cost_overwritten = true;
			}
		} 
	}
	
	if (cost == 0)
	{
		if (stops_are_family(stop_from, stop_to) < 0)
		{
			dist = query_get_distance_between_stops(stop_from, stop_to);
			cost = (int32_t) (dist * 655); // 5.5 km/h in s/km, average walking speed
		}
		else
			cost = (int32_t) default_cost * 60;
	}
	
	return cost;
}

// TODO min heap
size_t query_get_route
(
	stop_times_t *(*stop_times[]),
	stops_t *start,
	stops_t *dest,
	date_time_t start_time,
	const uint32_t tol_min,
	const uint32_t default_changing_time
)
{
	route_nodes_t **frontier;
	route_nodes_t **visited;
	
	route_nodes_t *node_f, *node_v, *node_tmp, **node_tmp2;
	stop_times_t **working_set;
	date_time_t curr_time;
	uint32_t new_cost;
	int32_t changing_cost;
	double waiting_cost, edge_cost;
	stop_times_t *stop_time, *stop_time_prev;
	route_nodes_t s_node;
	struct tm tm;
	
	size_t size_working_set;
	size_t num_elements_visited = 1;
	size_t num_elements_frontier = 1;
	size_t size_frontier = 1;
	size_t size_visited = 1;
	size_t cntr = 0;
	
	//start_time = date_time_t_sub_sec(start_time, tol_min * 60 / 2);
	*stop_times = NULL;
	
	// Add starting point to frontier list
	frontier = ecalloc(1, sizeof *frontier);
	node_f = ecalloc(1, sizeof *node_f);
	node_f->stop = start;
	node_f->priority = 0;
	frontier[0] = node_f;
	
	// Add starting point to visited list
	visited = ecalloc(1, sizeof *visited);
	node_v = ecalloc(1, sizeof *node_v);
	node_v->stop = start;
	node_v->cost_so_far = 0;
	node_v->came_from = NULL;
	node_v->came_with = NULL;
	visited[0] = node_v;
	
	// While there are still points to explore
	while (num_elements_frontier > 0)
	{
		// Pop closest node
		num_elements_frontier--;
		node_f = frontier[num_elements_frontier];
		frontier[num_elements_frontier] = NULL;
		
		// Get node properties
		node_v = *(route_nodes_t **) bsearch(
			&node_f,
			visited,
			num_elements_visited,
			sizeof *visited,
			HT_CMP_DATA(route_nodes_cmp_by_stop));
		
		// Calculate the current time based on cost and starting time
		curr_time = date_time_t_add_sec(start_time, node_v->cost_so_far);
		tm = *localtime_r(&curr_time, &tm);
		tm.tm_mday = 0;
		tm.tm_mon = 0;
		tm.tm_year = 0;
		tm.tm_wday = 0;
		tm.tm_yday = 0;
		tm.tm_isdst = 0;
			
		// Destination reached
		if (stops_are_family(node_f->stop, dest) >= 0)
		{
			efree(node_f);
			break;
		}
		
		// Get neighbours (including stops in the vicinity)
		size_working_set = query_get_connecting_trips(
			&working_set,
			node_v->stop,
			node_v->came_with,
			curr_time,
			tol_min);
		
		// Loop through all neighbours to see which one can be visited
		#pragma omp parallel for private(stop_time, stop_time_prev, tm, waiting_cost, edge_cost, changing_cost, new_cost, s_node, node_tmp2, node_tmp)
		for (size_t i = 0; i < size_working_set; i++)
		{
			stop_time = working_set[i];
			stop_time_prev = stop_time->trip->stop_times[stop_time->stop_sequence - 1];
			
			// Calculate additional cost to neighbour
			waiting_cost = difftime(stop_time_prev->departure_time, mktime(&tm));
			edge_cost = difftime(stop_time->arrival_time, stop_time_prev->departure_time);
			
			assert(edge_cost >= 0);
			assert(waiting_cost >= 0);
			
			// Calculate changing cost based on default and stop_times
			changing_cost = route_changing_cost(node_v->stop, node_v->came_with, stop_time_prev, default_changing_time);
			assert(changing_cost >= 0);
			
			new_cost = node_v->cost_so_far + (uint32_t) (waiting_cost + edge_cost);
			
			// Check if next node has already been visited
			s_node.stop = stop_time->stop;
			node_tmp2 = bsearch(
				&(route_nodes_t *){&s_node},
				visited,
				num_elements_visited,
				sizeof *visited,
				HT_CMP_DATA(route_nodes_cmp_by_stop));
			
			// If either the node hasn't been visited yet (quickly malloc
			// it then) OR the current route is shorter than the previous one	
			if (
				(node_tmp2 == NULL && 
					(node_tmp = ecalloc(1, sizeof *node_tmp)))
				|| (new_cost < (*node_tmp2)->cost_so_far &&
					(node_tmp = *node_tmp2)))
			{
				// Add neighbour to visited list...
				node_tmp->stop = stop_time->stop;
				node_tmp->cost_so_far = new_cost;
				node_tmp->came_from = node_v;
				node_tmp->came_with = stop_time;
				
				//... but only if it hasn't been added before
				if (node_tmp2 == NULL)
				{
					#pragma omp critical(queries_get_route1)
					{
						// Enlarge list if necessary
						if (num_elements_visited >= size_visited)
						{
							size_visited *= 2;
							visited = erealloc(visited, size_visited, sizeof *visited);
						}
						visited[num_elements_visited] = node_tmp;
						num_elements_visited++;
						
						// Sort for bsearch
						qsort(visited, num_elements_visited, sizeof *visited, HT_CMP_DATA(route_nodes_cmp_by_stop));
					}
				}
				
				// Add neighbour to frontier list
				node_tmp = ecalloc(1, sizeof *node_tmp);
				node_tmp->stop = stop_time->stop;
				node_tmp->priority = new_cost + route_estimate_cost(stop_time->stop, dest);
				
				#pragma omp critical(queries_get_route2)
				{
					// Enlarge list if necessary
					if (num_elements_frontier >= size_frontier)
					{
						size_frontier *= 2;
						frontier = erealloc(frontier, size_frontier, sizeof *frontier);
					}
					frontier[num_elements_frontier] = node_tmp;
					num_elements_frontier++;
					
					// Sort so lowest prio is at the end
					qsort(frontier, num_elements_frontier, sizeof *frontier, HT_CMP_DATA(route_nodes_cmp_by_dist));
				}
			}
		}
		efree(working_set);
		
		efree(node_f);
	}
	
	// Free frontier list
	for (size_t i = 0; i < num_elements_frontier; i++)
		efree(frontier[i]);
	efree(frontier);
	
	// Search dest node and walk path backwards
	s_node.stop = dest;
	node_tmp2 = bsearch(
		&(route_nodes_t *){&s_node},
		visited,
		num_elements_visited,
		sizeof *visited,
		HT_CMP_DATA(route_nodes_cmp_by_stop));
	if (node_tmp2 != NULL)
	{
		node_v = *node_tmp2;
		while (node_v != NULL)
		{
			cntr++;
			*stop_times = erealloc(*stop_times, cntr, sizeof **stop_times);
			(*stop_times)[cntr - 1] = node_v->came_with;
			node_v = node_v->came_from;
		}
	}
	
	FILE *stations = efopen("./stations.dot", "w");
	fprintf(stations, "digraph {\nnode [style=filled,shape=box]\n");
	fprintf(stations, "\"%s\" [pos=\"%u,%u!\"];\n",
		start->name,
		(uint32_t) (start->lon * 500) % 500,
		(uint32_t) (start->lat * 500) % 500);
	
	for (size_t i = 0; i < num_elements_visited; i++)
	{
		if (visited[i]->came_with != NULL)
		{
			fprintf(stations, "\"%s\" [pos=\"%u,%u!\"];\n",
				visited[i]->stop->name,
				(uint32_t) (visited[i]->stop->lon * 500) % 500,
				(uint32_t) (visited[i]->stop->lat * 500) % 500);
			
			fprintf(stations, "\"%s\" [pos=\"%u,%u!\"];\n",
				visited[i]->came_from->stop->name,
				(uint32_t) (visited[i]->came_from->stop->lon * 500) % 500,
				(uint32_t) (visited[i]->came_from->stop->lat * 500) % 500);
					
			fprintf(stations, "\"%s\" -> \"%s\" [label=\"%s (%u)\"];\n", 
				visited[i]->came_from->stop->name,
				visited[i]->stop->name,
				visited[i]->came_with->trip->route->short_name,
				visited[i]->cost_so_far);
		}
		else if (visited[i]->came_from == NULL)
		{
			fprintf(stations, "\"%s\" [pos=\"%u,%u!\"];\n",
				visited[i]->stop->name,
				(uint32_t) (visited[i]->stop->lon * 500) % 500,
				(uint32_t) (visited[i]->stop->lat * 500) % 500);
			
			fprintf(stations, "\"%s\" -> \"%s\" [label=\"%s (%u)\"];\n", 
				start->name,
				visited[i]->stop->name,
				"---",
				visited[i]->cost_so_far);
		}
	}
	fprintf(stations, "}\n");
	efclose(stations);
	
	// Free visited list
	for (size_t i = 0; i < num_elements_visited; i++)
		efree(visited[i]);
	efree(visited);
	
	return cntr;
}

////////////////////////////////

static inline void loop_stops
(
	const stops_t *stop, 
	stop_times_t *(*stop_times[]), 
	size_t *num_stop_times,
	const date_time_t time, 
	const uint32_t tol_min
)
{
	struct tm tm;
	stop_times_t *stop_time;
	
	tm = *localtime_r(&time, &tm);
	tm.tm_mday = 0;
	tm.tm_mon = 0;
	tm.tm_year = 0;
	tm.tm_wday = 0;
	tm.tm_yday = 0;
	tm.tm_isdst = 0;
	
	#pragma omp parallel for private(stop_time, tm)
	for (size_t j = 0; j < stop->num_stop_times; j++)
	{
		stop_time = stop->stop_times[j];
		if (query_trip_running(stop_time->trip, time) && 
			fabs(difftime(mktime(&tm), stop_time->departure_time)) <= tol_min * 60)
		{
			// TODO can also be merged later, faster?
			#pragma omp critical(queries_get_trips_at_station_with_departure)
			{
				(*num_stop_times)++;
				*stop_times = erealloc(*stop_times, *num_stop_times, sizeof **stop_times);
				(*stop_times)[*num_stop_times - 1] = stop_time;
			}
		}
	}
}

size_t query_get_trips_at_station_with_departure
(
	stop_times_t *(*stop_times[]),
	const stops_t *stop,
	const date_time_t time,
	const uint32_t tol_min,
	bool sort
)
{
	*stop_times = NULL;
	const stops_t *parent;
	size_t num_stop_times = 0;
	
	if (stop->parent_station == NULL) parent = stop;
	else parent = stop->parent_station;
	
	loop_stops(parent, stop_times, &num_stop_times, time, tol_min);
	for (size_t i = 0; i < parent->num_children_stations; i++)
	{
		loop_stops(parent->children_stations[i], stop_times, &num_stop_times, time, tol_min);
	}
	
	if (sort)
		qsort(*stop_times, num_stop_times, sizeof **stop_times, HT_CMP_DATA(&stop_times_cmp));
	return num_stop_times;
}

////////////////////////////////

size_t query_get_connecting_trips
(
	stop_times_t *(*stop_times[]),
	const stops_t *stop,
	const stop_times_t *came_with,
	const date_time_t time,
	const uint32_t future_timeframe_min
)
{
	stop_times_t **stop_times_tmp;
	stops_t **stops_vic;
	size_t num_stop_times, num_stops_vic, num_trips = 0;
	stop_times_t *st;
	unsigned int seq_next;
	int32_t cost;
	date_time_t med_timeframe_time = date_time_t_add_sec(time, (uint32_t) (future_timeframe_min * 60));
	
	*stop_times = NULL;
	
	num_stops_vic = query_get_stops_in_vicinity(
		&stops_vic,
		stop->lat,
		stop->lon,
		0.5f);
	for (size_t i = 0; i < num_stops_vic; i++)
	{
		num_stop_times = query_get_trips_at_station_with_departure(
		&stop_times_tmp,
		stops_vic[i],
		med_timeframe_time,
		future_timeframe_min,
		false);
		
		#pragma omp parallel for private(st, seq_next, cost)
		for (size_t j = 0; j < num_stop_times; j++)
		{
			st = stop_times_tmp[j];
			seq_next = st->stop_sequence + 1;
			
			// Ignore stations that were in the vicinity but on the same trip
			if ((came_with != NULL && came_with->trip == st->trip &&
				came_with->stop_sequence + 1 != seq_next) || 
				(st->stop_sequence > 0 && 
				stops_are_family(st->trip->stop_times[st->stop_sequence - 1]->stop, stop) >= 0))
				break;
			
			cost = route_changing_cost(stop, came_with, st, 2);
			if (cost < 0 || (came_with != NULL && date_time_t_add_sec(came_with->arrival_time, (uint32_t) cost) > st->departure_time))
				continue;
			
			if (seq_next < st->trip->num_stop_times)
			{
				#pragma omp critical(queries_get_connecting_trips)
				{
					num_trips++;
					*stop_times = erealloc(*stop_times, num_trips, sizeof **stop_times);
					(*stop_times)[num_trips - 1] = st->trip->stop_times[seq_next];
				}
			}
		}
		efree(stop_times_tmp);
	}
	efree(stops_vic);
	
	return num_trips;
}

////////////////////////////////

bool query_trip_running(const trips_t *trip, date_time_t time)
{
	calendar_t *service;
	bool ret;
	struct tm tm, tm2;
	calendar_service_availability_t avail;
	calendar_dates_t **excs;
	calendar_dates_exception_t excp;
	size_t num_excs;
	
	service = trip->service;
	ret = difftime(service->end_date, time) > 0 && difftime(time, service->start_date) > 0;
	switch (localtime_r(&time, &tm)->tm_wday)
	{
		case 0:	// Sunday
			avail = service->sunday;
			break;
		
		case 1:	// Monday
			avail = service->monday;
			break;
			
		case 2:	// Tuesday
			avail = service->tuesday;
			break;
			
		case 3:	// Wednesday
			avail = service->wednesday;
			break;
			
		case 4:	// Thursday
			avail = service->thursday;
			break;
			
		case 5:	// Friday
			avail = service->friday;
			break;
			
		case 6:	// Saturday
			avail = service->saturday;
			break;
			
		default:
			fprintf(stderr, "Day-of-week %d unknown!\n", localtime_r(&time, &tm)->tm_wday);
			return false;
			break;
	}
	
	if (avail == SERVICE_AVAILABLE) ret |= true;
	if (avail == SERVICE_NOT_AVAILABLE) ret &= false;
	
	tm = *localtime_r(&time, &tm);
	excp = UNKNOWN_SERVICE;
	
	num_excs = list_get_elements(&service->exceptions, &excs);
	for (size_t i = 0; i < num_excs; i++)
	{
		tm2 = *localtime_r(&excs[i]->date, &tm2);
		
		if (tm.tm_mday == tm2.tm_mday &&
			tm.tm_mon == tm2.tm_mon && 
			tm.tm_year == tm2.tm_year)
		{
			excp = excs[i]->exception_type;
			break; // Should be possible, more than one exception doesn't make sense
		}
	}
	efree(excs);
	
	if (excp == ADDED_SERVICE) ret |= true;
	if (excp == REMOVED_SERVICE) ret &= false;
	
	return ret;
}

////////////////////////////////

double query_get_distance_between_stops(const stops_t *stop1, const stops_t *stop2)
{
	double lat1 = stop1->lat * M_PI / 180.0;
	double lat2 = stop2->lat * M_PI / 180.0;
	double lon1 = stop1->lon * M_PI / 180.0;
	double lon2 = stop2->lon * M_PI / 180.0;
	double dlon = lon2 - lon1;
	double dlat = lat2 - lat1;
	double a = pow(sin(dlat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlon / 2), 2);
	
	return 2 * 6373.0 * atan2(sqrt(a), sqrt(1 - a));
}
