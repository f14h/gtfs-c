#ifndef _QUERIES_H_
#define _QUERIES_H_

#include <stdbool.h>
#include <stdint.h>

#include "agency.h"
#include "stops.h"
#include "routes.h"
#include "trips.h"
#include "stop_times.h"
#include "calendar.h"
#include "transfers.h"

#include "types.h"
#include "gtfs.h"

hashtable_t *query_get_database(gtfs_dataset_t dataset);
const value_t *query_lookup_by_id(gtfs_dataset_t dataset, const char *key);
const void *query_lookup_data_by_id(gtfs_dataset_t dataset, const char *key);

size_t query_get_stops_with_name(stops_t *(*stops[]), const char *name);
size_t query_get_stops_in_vicinity(stops_t *(*stops[]), float lat, float lon, float dist);

size_t query_get_route(stop_times_t *(*stop_times[]), stops_t *start, stops_t *dest, date_time_t start_time, const uint32_t tol_min, const uint32_t default_changing_time);
size_t query_get_trips_at_station_with_departure(stop_times_t *(*stop_times[]), const stops_t *stop, const date_time_t time, const uint32_t tol_min, bool sort);
size_t query_get_connecting_trips(stop_times_t *(*stop_times[]), const stops_t *stop, const stop_times_t *coming_from, const date_time_t time, const uint32_t future_timeframe_min);

bool query_trip_running(const trips_t *trip, date_time_t time);
double query_get_distance_between_stops(const stops_t *stop1, const stops_t *stop2);

#endif //_QUERIES_H_
