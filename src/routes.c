#include "routes.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <csv.h>
#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "utils.h"
#include "debug.h"

const char *ROUTES_FIELDS[] = 
{
	"NULL",
	"route_id",
	"agency_id",
	"route_short_name",
	"route_long_name",
	"route_desc",
	"route_type",
	"route_url",
	"route_color",
	"route_text_color",
	"route_sort_order",
	"continuous_pickup",
	"continuous_drop_off"
};

typedef struct
{
	size_t field;
	ssize_t row_cntr;
	routes_t *route;
	const gtfs_database_t *db;
	routes_fields_t cols_map[routes_fields_t_NUM_MEMBERS];
} routes_csv_parser_data_t;

void routes_print_compact(const char *title, const routes_t *routes, const char *fallback)
{
	assert(title != NULL);
	assert(fallback != NULL);
	
	if (routes == NULL)
		printf("%s: %s\n", title, fallback);
	else
		printf("%s: [%s] %s (%s)\n", title, routes->agency->name, routes->short_name != NULL ? routes->short_name : routes->long_name, routes->id);
}

void routes_print(const routes_t *routes)
{
	#pragma omp critical(routes_print)
	{
		assert(routes != NULL);
		
		printf("--------------------\n");
		printf("route_id: %s\n", routes->id);
		agency_print_compact("agency", routes->agency, routes->agency_id);
		printf("route_short_name: %s\n", routes->short_name);
		printf("route_long_name: %s\n", routes->long_name);
		printf("route_desc: %s\n", routes->desc);
		printf("route_type: %d\n", routes->type);
		printf("route_url: %s\n", routes->url);
		printf("route_color: #%06X\n", routes->color);
		printf("route_text_color: #%06X\n", routes->text_color);
		printf("route_sort_order: %u\n", routes->route_sort_order);
		printf("continuous_pickup: %d\n", routes->continuous_pickup);
		printf("continuous_drop_off: %d\n", routes->continuous_drop_off);
		printf("--------------------\n");
	}
}

int routes_cmp(routes_t * const *route1, routes_t * const *route2)
{
	if (*route1 == NULL && *route2 == NULL) return 0;
	if (*route1 == NULL) return -1;
	if (*route2 == NULL) return 1;
	
	if (*route1 == *route2) return 0;
	
	int r = agency_cmp(&(*route1)->agency, &(*route2)->agency);
	
	if (r == 0)
	{
		if ((*route1)->short_name != NULL && (*route2)->short_name != NULL)
			r = strcmp((*route1)->short_name, (*route2)->short_name);
		
		if (r == 0)
		{
			if ((*route1)->long_name != NULL && (*route2)->long_name != NULL)
				r = strcmp((*route1)->long_name, (*route2)->long_name);
			
			if (r == 0)
				r = INTCMP((*route1)->route_sort_order, (*route2)->route_sort_order);
		}
	}
	
	return r;
}

void routes_free(routes_t **routes)
{
	routes_t *route;
	
	assert(routes != NULL);
	assert(*routes != NULL);
	
	route = *routes;
		
	efree(route->id);
	efree(route->agency_id);
	efree(route->short_name);
	efree(route->long_name);
	efree(route->transfers);
	efree(route->desc);
	efree(route->url);
	efree(route);
	
	*routes = NULL;
}

static void cb1(char *field, size_t size, routes_csv_parser_data_t *p)
{
	ssize_t index;
	
	if (p->row_cntr == 0)
	{
		index = get_index(ROUTES_FIELDS, field, routes_fields_t_NUM_MEMBERS);
		if (index >= 0)
		{
			p->cols_map[p->field] = (routes_fields_t) index;
		}
		else
			fprintf(stderr, "Error reading ROUTES file: Unknown header <%s>\n", field);
	}
	else
	{
		switch (p->cols_map[p->field])
		{
			case ROUTE_ID:
				p->route->id = estrdup(field);
				break;
				
			case ROUTE_AGENCY_ID:
				p->route->agency_id = estrdup(field);
				p->route->agency = hashtable_get_data(p->db->agency, p->route->agency_id);
				break;
				
			case ROUTE_SHORT_NAME:
				p->route->short_name = estrdup(field);
				break;
				
			case ROUTE_LONG_NAME:
				p->route->long_name = estrdup(field);
				break;
				
			case ROUTE_DESC:
				p->route->desc = estrdup(field);
				break;
				
			case ROUTE_TYPE:
				p->route->type = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case ROUTE_URL:
				p->route->url = estrdup(field);
				break;
				
			case ROUTE_COLOR:
				sscanf(field, "%06X", &p->route->color);
				break;
				
			case ROUTE_TEXT_COLOR:
				sscanf(field, "%06X", &p->route->text_color);
				break;
				
			case ROUTE_SORT_ORDER:
				p->route->route_sort_order = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case ROUTE_CONTINUOUS_PICKUP:
				p->route->continuous_pickup = (unsigned int) estrtol(field, NULL, 10, ROUTES_CONTINUOUS);
				break;
				
			case ROUTE_CONTINUOUS_DROP_OFF:
				p->route->continuous_drop_off = (unsigned int) estrtol(field, NULL, 10, ROUTES_CONTINUOUS);
				break;
				
			default:
				fprintf(stderr, "Error creating ROUTE: Unknown field %d\n", p->cols_map[p->field]);
				break;
		}
	}
	
	p->field++;
}

static void cb2(int chr, routes_csv_parser_data_t *p)
{
	if (p->row_cntr > 0)
		hashtable_set
		(
			p->db->routes,
			p->route->id,
			p->route,
			HT_CMP_DATA(&routes_cmp),
			HT_PRINT_DATA(&routes_print),
			HT_FREE_DATA(&routes_free)
		);	
	
	p->field = 0;
	p->row_cntr++;
	
	p->route = ecalloc(1, sizeof *p->route);
}

void routes_add_transfer(routes_t *route, transfers_t *transfer)
{
	if (route == NULL || transfer == NULL) return;
	
	#pragma omp critical(routes_add_transfer)
	{
		route->num_transfers++;
		route->transfers = erealloc(route->transfers, route->num_transfers, sizeof *route->transfers);
		route->transfers[route->num_transfers - 1] = transfer;
	}
}

ssize_t routes_read_file(FILE *fp, gtfs_database_t *db)
{
	size_t bytes_read, retval, pos = 0;
	struct csv_parser cp;
	char buf[1024];
	
	printf("Reading ROUTES...\n");
	db->routes = hashtable_init(256);
	
	routes_csv_parser_data_t data;
	data.field = 0;
	data.row_cntr = 0;
	data.route = NULL;
	data.db = db;
	memset(data.cols_map, 0, sizeof data.cols_map);
	
	csv_init(&cp, CSV_APPEND_NULL | CSV_EMPTY_IS_NULL);
	
	while ((bytes_read = fread(buf, 1, sizeof buf, fp)) > 0)
	{
		if ((retval = csv_parse(&cp, buf, bytes_read, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data)) != bytes_read)
		{
			if (csv_error(&cp) == CSV_EPARSE) 
			{
				printf("ROUTES malformed at byte %lu\n", (unsigned long) pos + retval + 1);
				data.row_cntr = -1;
				
				goto end;
			}
			else
			{
				printf("Error while processing ROUTES: %s\n", csv_strerror(csv_error(&cp)));
				data.row_cntr = -1;
				
				goto end;
			}
		}
		pos += bytes_read;
	}
	
end:
	csv_fini(&cp, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data);
	csv_free(&cp);

	efree(data.route);

	return data.row_cntr;
}
