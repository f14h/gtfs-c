#ifndef _ROUTES_H_
#define _ROUTES_H_

#include <stdio.h>
#include <sys/types.h>

#include "types.h"
#include "hashtable.h"

#include "agency.h"

typedef enum
{
	ROUTES_TRAM_STREETCAR_LIGHTRAIL = 0,
	ROUTES_SUBWAY_METRO = 1,
	ROUTES_RAIL = 2,
	ROUTES_BUS = 3,
	ROUTES_FERRY = 4,
	ROUTES_CABLE_TRAM = 5,
	ROUTES_AERIAL_LIFT = 6,
	ROUTES_FUNICULAR = 7,
	ROUTES_TROLLEYBUS = 11,
	ROUTES_MONORAIL = 12
} routes_type_t;

typedef enum
{
	ROUTES_CONTINUOUS = 0,
	ROUTES_NO_CONTINUOUS = 1,
	ROUTES_MUST_PHONE = 2,
	ROUTES_MUST_COORDINATE_DRIVER = 3
} continuous_pickup_dropoff_t;

struct _routes_t
{
	gtfs_id_t id;
	gtfs_id_t agency_id;
	agency_t *agency;
	text_t short_name;
	text_t long_name;
	text_t desc;
	size_t num_transfers;
	transfers_t **transfers;
	routes_type_t type;
	url_t url;
	color_t color;
	color_t text_color;
	unsigned int route_sort_order;
	continuous_pickup_dropoff_t continuous_pickup;
	continuous_pickup_dropoff_t continuous_drop_off;
};

typedef enum
{
	ROUTE_ID = 1,
	ROUTE_AGENCY_ID,
	ROUTE_SHORT_NAME,
	ROUTE_LONG_NAME,
	ROUTE_DESC,
	ROUTE_TYPE,
	ROUTE_URL,
	ROUTE_COLOR,
	ROUTE_TEXT_COLOR,
	ROUTE_SORT_ORDER,
	ROUTE_CONTINUOUS_PICKUP,
	ROUTE_CONTINUOUS_DROP_OFF,
	routes_fields_t_NUM_MEMBERS
} routes_fields_t;

extern const char *ROUTES_FIELDS[];

void routes_print_compact(const char *title, const routes_t *routes, const char *fallback);
void routes_print(const routes_t *routes);
void routes_free(routes_t **routes);
int routes_cmp(routes_t * const *route1, routes_t * const *route2);
void routes_add_transfer(routes_t *route, transfers_t *transfer);
ssize_t routes_read_file(FILE *fp, gtfs_database_t *db);

#endif //_ROUTES_H_
