#include "stop_times.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <csv.h>
#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "utils.h"
#include "debug.h"

#define TID omp_get_thread_num()
#define Thds omp_get_max_threads()

const char *STOP_TIMES_FIELDS[] = 
{
	"NULL",
	"trip_id",
	"arrival_time",
	"departure_time",
	"stop_id",
	"stop_sequence",
	"stop_headsign",
	"pickup_type",
	"drop_off_type",
	"continuous_pickup",
	"continuous_drop_off",
	"shape_dist_traveled",
	"timepoint"
};

typedef struct
{
	size_t *field;
	ssize_t row_cntr;
	stop_times_t **stop_time;
	const gtfs_database_t *db;
	stop_times_fields_t cols_map[stop_times_fields_t_NUM_MEMBERS];
} stop_times_csv_parser_data_t;

void stop_times_print(const stop_times_t *stop_times)
{
	assert(stop_times != NULL);
		
	#pragma omp critical(stop_times_print)
	{
		printf("--------------------\n");
		trips_print_compact("trip", stop_times->trip, stop_times->trip_id);
		printf("arrival_time: %s\n", date_time_t_to_str("%H:%M:%S", stop_times->arrival_time));
		printf("departure_time: %s\n", date_time_t_to_str("%H:%M:%S", stop_times->departure_time));
		stops_print_compact("stop", stop_times->stop, stop_times->stop_id);
		printf("stop_sequence: %u\n", stop_times->stop_sequence);
		printf("stop_headsign: %s\n", stop_times->stop_headsign);
		printf("pickup_type: %d\n", stop_times->pickup_type);
		printf("drop_off_type: %d\n", stop_times->drop_off_type);
		printf("continuous_pickup: %d\n", stop_times->continuous_pickup);
		printf("continuous_drop_off: %d\n", stop_times->continuous_drop_off);
		printf("shape_dist_traveled: %f\n", stop_times->shape_dist_traveled);
		printf("timepoint: %d\n", stop_times->timepoint);
		printf("--------------------\n");
	}
}

int stop_times_cmp(stop_times_t * const *stop_time1, stop_times_t * const *stop_time2)
{
	if (*stop_time1 == NULL && *stop_time2 == NULL) return 0;
	if (*stop_time1 == NULL) return -1;
	if (*stop_time2 == NULL) return 1;
	
	if (*stop_time1 == *stop_time2) return 0;
	
	int r = trips_cmp(&(*stop_time1)->trip, &(*stop_time2)->trip);
	if (r == 0)
		r = INTCMP((*stop_time1)->stop_sequence, (*stop_time2)->stop_sequence);
		
	return r;
}

void stop_times_free(stop_times_t **stop_times)
{
	stop_times_t *stop_time;
	
	assert(stop_times != NULL);
	assert(*stop_times != NULL);
	
	stop_time = *stop_times;
		
	efree(stop_time->trip_id);
	efree(stop_time->stop_id);
	efree(stop_time->stop_headsign);
	efree(stop_time);
	
	*stop_times = NULL;
}

static void cb1(char *field, size_t size, stop_times_csv_parser_data_t *p)
{
	ssize_t index;
	
	if (p->row_cntr == 0)
	{
		index = get_index(STOP_TIMES_FIELDS, field, stop_times_fields_t_NUM_MEMBERS);
		if (index >= 0)
		{
			p->cols_map[p->field[TID]] = (stop_times_fields_t) index;
		}
		else
			fprintf(stderr, "Error reading STOP_TIMES file: Unknown header <%s>\n", field);
	}
	else
	{
		switch (p->cols_map[p->field[TID]])
		{
			case STOP_TIME_TRIP_ID:
				p->stop_time[TID]->trip_id = estrdup(field);
				p->stop_time[TID]->trip = hashtable_get_data(p->db->trips, p->stop_time[TID]->trip_id);
				trips_add_stop_time(p->stop_time[TID]->trip, p->stop_time[TID]);
				break;
				
			case STOP_TIME_ARRIVAL_TIME:
				p->stop_time[TID]->arrival_time = str_to_date_time_t("%T", field);
				break;
				
			case STOP_TIME_DEPARTURE_TIME:
				p->stop_time[TID]->departure_time = str_to_date_time_t("%T", field);
				break;
				
			case STOP_TIME_STOP_ID:
				p->stop_time[TID]->stop_id = estrdup(field);
				p->stop_time[TID]->stop = hashtable_get_data(p->db->stops, p->stop_time[TID]->stop_id);
				stops_add_stop_time(p->stop_time[TID]->stop, p->stop_time[TID]);
				break;
				
			case STOP_TIME_STOP_SEQUENCE:
				p->stop_time[TID]->stop_sequence = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case STOP_TIME_STOP_HEADSIGN:
				p->stop_time[TID]->stop_headsign = estrdup(field);
				break;
				
			case STOP_TIME_PICKUP_TYPE:
				p->stop_time[TID]->pickup_type = (unsigned int) estrtol(field, NULL, 10, STOP_TIMES_REGULARLY);
				break;
				
			case STOP_TIME_DROP_OFF_TYPE:
				p->stop_time[TID]->drop_off_type = (unsigned int) estrtol(field, NULL, 10, STOP_TIMES_REGULARLY);
				break;
				
			case STOP_TIME_CONTINUOUS_PICKUP:
				p->stop_time[TID]->continuous_pickup = (unsigned int) estrtol(field, NULL, 10, ROUTES_CONTINUOUS);
				break;
				
			case STOP_TIME_CONTINUOUS_DROP_OFF:
				p->stop_time[TID]->continuous_drop_off = (unsigned int) estrtol(field, NULL, 10, ROUTES_CONTINUOUS);
				break;
				
			case STOP_TIME_SHAPE_DIST_TRAVELED:
				p->stop_time[TID]->shape_dist_traveled = strtof(field, NULL);
				break;
				
			case STOP_TIME_TIMEPOINT:
				p->stop_time[TID]->timepoint = (unsigned int) estrtol(field, NULL, 10, STOP_TIMES_EXACT);
				break;
				
			default:
				fprintf(stderr, "Error creating STOP_TIMES: Unknown field %d\n", p->cols_map[p->field[TID]]);
				break;
		}
	}
	
	p->field[TID]++;
}

static void cb2(int chr, stop_times_csv_parser_data_t *p)
{
	char buf[64];
	
	if (p->row_cntr > 0)
	{
		snprintf(buf, sizeof buf, "%s%s", p->stop_time[TID]->trip_id, p->stop_time[TID]->stop_id);
		
		hashtable_set
		(
			p->db->stop_times,
			buf,
			p->stop_time[TID],
			HT_CMP_DATA(&stop_times_cmp),
			HT_PRINT_DATA(&stop_times_print),
			HT_FREE_DATA(&stop_times_free)
		);
	}
	else
	{
		for (int i = 1; i < omp_get_num_threads(); i++)
		{
			p->stop_time[i] = ecalloc(1, sizeof *p->stop_time[i]);
		}
	}
	
	p->field[TID] = 0;
	
	#pragma omp atomic update
	p->row_cntr++;
	
	p->stop_time[TID] = ecalloc(1, sizeof *p->stop_time[TID]);
}

ssize_t stop_times_read_file(FILE *fp, gtfs_database_t *db)
{
	size_t bytes_read, retval, pos = 0;
	struct csv_parser cp;
	char *res;
	char buf[1024];
	
	printf("Reading STOP_TIMES...\n");
	db->stop_times = hashtable_init(32768);
	
	stop_times_csv_parser_data_t data;
	data.field = ecalloc((size_t) Thds, sizeof *data.field);
	data.row_cntr = 0;
	data.stop_time = ecalloc((size_t) Thds, sizeof *data.stop_time);
	data.db = db;
	memset(data.cols_map, 0, sizeof data.cols_map);
	
	#pragma omp parallel private(buf, cp, res, bytes_read, retval)
	{
	bytes_read = 0;
	csv_init(&cp, CSV_APPEND_NULL | CSV_EMPTY_IS_NULL);
	
	while (1)
	{
		while (TID != 0 && data.row_cntr == 0)
		;
		
		#pragma omp critical(stop_times_read_line)
		{
			pos += bytes_read;
			res = fgets(buf, sizeof buf, fp);
		}
		if (res == NULL) break;
		bytes_read = strlen(buf);
		
		if ((retval = csv_parse(&cp, buf, bytes_read, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data)) != bytes_read)
		{
			if (csv_error(&cp) == CSV_EPARSE) 
			{
				printf("STOP_TIMES malformed at byte %lu\n", (unsigned long) pos + retval + 1);
				
				break;
			}
			else
			{
				printf("Error while processing STOP_TIMES: %s\n", csv_strerror(csv_error(&cp)));
				
				break;
			}
		}
	}
	
	csv_fini(&cp, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data);
	csv_free(&cp);

	efree(data.stop_time[TID]);
	}
	
	efree(data.stop_time);
	efree(data.field);
	
	trips_sort_stop_times(db, &stop_times_cmp);
	stops_sort_stop_times(db, &stop_times_cmp);
	
	return data.row_cntr;
}
