#ifndef _STOP_TIMES_H_
#define _STOP_TIMES_H_

#include <stdio.h>
#include <sys/types.h>

#include "types.h"
#include "hashtable.h"

#include "routes.h"
#include "stops.h"
#include "trips.h"

typedef enum
{
	STOP_TIMES_APPROXIMATE = 0,
	STOP_TIMES_EXACT = 1
} stop_times_timepoint_t;

typedef enum
{
	STOP_TIMES_REGULARLY = 0,
	STOP_TIMES_NOT_AVAILABLE = 1,
	STOP_TIMES_MUST_PHONE = 2,
	STOP_TIMES_MUST_COORDINATE_DRIVER = 3,
} stop_times_pickup_dropoff_type_t;

struct _stop_times_t
{
	gtfs_id_t trip_id;
	trips_t *trip;
	date_time_t arrival_time;
	date_time_t departure_time;
	gtfs_id_t stop_id;
	stops_t *stop;
	unsigned int stop_sequence;
	text_t stop_headsign;
	stop_times_pickup_dropoff_type_t pickup_type;
	stop_times_pickup_dropoff_type_t drop_off_type;
	continuous_pickup_dropoff_t continuous_pickup;
	continuous_pickup_dropoff_t continuous_drop_off;
	float shape_dist_traveled;
	stop_times_timepoint_t timepoint;
};

typedef enum
{
	STOP_TIME_TRIP_ID = 1,
	STOP_TIME_ARRIVAL_TIME,
	STOP_TIME_DEPARTURE_TIME,
	STOP_TIME_STOP_ID,
	STOP_TIME_STOP_SEQUENCE,
	STOP_TIME_STOP_HEADSIGN,
	STOP_TIME_PICKUP_TYPE,
	STOP_TIME_DROP_OFF_TYPE,
	STOP_TIME_CONTINUOUS_PICKUP,
	STOP_TIME_CONTINUOUS_DROP_OFF,
	STOP_TIME_SHAPE_DIST_TRAVELED,
	STOP_TIME_TIMEPOINT,
	stop_times_fields_t_NUM_MEMBERS
} stop_times_fields_t;

extern const char *STOP_TIMES_FIELDS[];

void stop_times_print(const stop_times_t *stop_times);
int stop_times_cmp(stop_times_t * const *stop_time1, stop_times_t * const *stop_time2);
void stop_times_free(stop_times_t **stop_times);
ssize_t stop_times_read_file(FILE *fp, gtfs_database_t *db);

#endif //_STOP_TIMES_H_
