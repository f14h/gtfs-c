#include "stops.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <csv.h>
#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "utils.h"
#include "debug.h"

const char *STOPS_FIELDS[] = 
{
	"NULL",
	"stop_id",
	"stop_code",
	"stop_name",
	"stop_desc",
	"stop_lat",
	"stop_lon",
	"zone_id",
	"stop_url",
	"location_type",
	"parent_station",
	"stop_timezone",
	"wheelchair_boarding",
	"level_id",
	"platform_code"
};

typedef struct
{
	size_t field;
	ssize_t row_cntr;
	stops_t *stop;
	const gtfs_database_t *db;
	stops_fields_t cols_map[stops_fields_t_NUM_MEMBERS];
} stops_csv_parser_data_t;

void stops_print_compact(const char *title, const stops_t *stops, const char *fallback)
{
	assert(title != NULL);
	assert(fallback != NULL);
	
	if (stops == NULL)
		printf("%s: %s\n", title, fallback);
	else
		printf("%s: %s (%s)\n", title, stops->name, stops->id);
}

void stops_print(const stops_t *stops)
{
	#pragma omp critical(stops_print)
	{
		assert(stops != NULL);
		
		printf("--------------------\n");
		printf("stop_id: %s\n", stops->id);
		printf("stop_code: %s\n", stops->code);
		printf("stop_name: %s\n", stops->name);
		printf("num_stop_times: %zu\n", stops->num_stop_times);
		printf("stop_desc: %s\n", stops->desc);
		printf("stop_lat: %f\n", stops->lat);
		printf("stop_lon: %f\n", stops->lon);
		printf("zone_id: %s\n", stops->zone_id);
		printf("stop_url: %s\n", stops->url);
		printf("stop_location_type: %d\n", stops->location_type);
		stops_print_compact("parent_station", stops->parent_station, stops->parent_station_id == NULL ? "---" : stops->parent_station_id);
		printf("num_children_stations: %zu\n", stops->num_children_stations);
		printf("stop_timezone: %s\n", stops->timezone);
		printf("wheelchair_boarding: %d\n", stops->wheelchair_boarding);
		printf("level_id: %s\n", stops->level_id);
		printf("platform_code: %s\n", stops->platform_code);
		printf("--------------------\n");
	}
}

int stops_cmp(stops_t * const *stop1, stops_t * const *stop2)
{
	if (*stop1 == NULL && *stop2 == NULL) return 0;
	if (*stop1 == NULL) return -1;
	if (*stop2 == NULL) return 1;
	
	if (stops_are_family(*stop1, *stop2) >= 0) return 0;
	
	int r = strcmp((*stop1)->name, (*stop2)->name);
	if (r == 0)
		r = INTCMP((*stop2)->num_children_stations, (*stop1)->num_children_stations);
		
	return r;
}

void stops_free(stops_t **stops)
{
	stops_t *stop;
	
	assert(stops != NULL);
	assert(*stops != NULL);
	
	stop = *stops;
		
	efree(stop->id);
	efree(stop->code);
	efree(stop->name);
	efree(stop->stop_times);
	efree(stop->transfers);
	efree(stop->desc);
	efree(stop->zone_id);
	efree(stop->url);
	efree(stop->parent_station_id);
	efree(stop->children_stations);
	efree(stop->timezone);
	efree(stop->level_id);
	efree(stop->platform_code);
	efree(stop);
	
	*stops = NULL;
}

static void cb1(char *field, size_t size, stops_csv_parser_data_t *p)
{
	ssize_t index;
	
	if (p->row_cntr == 0)
	{
		index = get_index(STOPS_FIELDS, field, stops_fields_t_NUM_MEMBERS);
		if (index >= 0)
		{
			p->cols_map[p->field] = (stops_fields_t) index;
		}
		else
			fprintf(stderr, "Error reading STOPS file: Unknown header <%s>\n", field);
	}
	else
	{
		switch (p->cols_map[p->field])
		{
			case STOP_ID:
				p->stop->id = estrdup(field);
				break;
				
			case STOP_CODE:
				p->stop->code = estrdup(field);
				break;
				
			case STOP_NAME:
				p->stop->name = estrdup(field);
				break;
				
			case STOP_DESC:
				p->stop->desc = estrdup(field);
				break;
				
			case STOP_LAT:
				p->stop->lat = strtof(field, NULL);
				break;
				
			case STOP_LON:
				p->stop->lon = strtof(field, NULL);
				break;
				
			case STOP_ZONE_ID:
				p->stop->zone_id = estrdup(field);
				break;
				
			case STOP_URL:
				p->stop->url = estrdup(field);
				break;
				
			case STOP_LOC_TYPE:
				p->stop->location_type = (unsigned int) estrtol(field, NULL, 10, STOPS_STOP_PLATFORM);
				break;
				
			case STOP_PARENT_STATION:
				p->stop->parent_station_id = estrdup(field);
				p->stop->parent_station = NULL;
				break;
				
			case STOP_TIMEZONE:
				p->stop->timezone = estrdup(field);
				break;
			
			case STOP_WHEELCHAIR_BOARDING:
				p->stop->wheelchair_boarding = (unsigned int) estrtol(field, NULL, 10, STOPS_NO_INFO_INHERIT);
				break;
				
			case STOP_LEVEL_ID:
				p->stop->level_id = estrdup(field); // TODO lookup
				break;
				
			case STOP_PLATFORM_CODE:
				p->stop->platform_code = estrdup(field);
				break;
				
			default:
				fprintf(stderr, "Error creating STOP: Unknown field %d\n", p->cols_map[p->field]);
				break;
		}
	}
	
	p->field++;
}

static void cb2(int chr, stops_csv_parser_data_t *p)
{
	if (p->row_cntr > 0)
		hashtable_set
		(
			p->db->stops,
			p->stop->id,
			p->stop,
			HT_CMP_DATA(&stops_cmp),
			HT_PRINT_DATA(&stops_print),
			HT_FREE_DATA(&stops_free)
		);	
	
	p->field = 0;
	p->row_cntr++;
	
	p->stop = ecalloc(1, sizeof *p->stop);
}

static void cb_insert_ptr(stops_t *stop, const gtfs_database_t *db)
{
	stops_t *parent;
	
    if (stop->parent_station_id != NULL && strncmp(stop->parent_station_id, "", 5) != 0)
	{
		parent = hashtable_get(db->stops, stop->parent_station_id)->data;
		if (parent == NULL)
		{
			DPRINT_WARN("STOPS lookup parent station (%s) not found: %s\n", stop->id, stop->parent_station_id);
		}
		else
		{
			#pragma omp critical(stops_get_children_stations)
			{
				stop->parent_station = parent;
				parent->children_stations = erealloc(parent->children_stations, (parent->num_children_stations + 1), sizeof *parent->children_stations);
				parent->children_stations[parent->num_children_stations++] = stop;
			}
		}
	}
}

void stops_add_stop_time(stops_t *stop, stop_times_t *stop_time)
{
	if (stop == NULL || stop_time == NULL) return;
	
	#pragma omp critical(stops_add_stop_time)
	{
		stop->num_stop_times++;
		stop->stop_times = erealloc(stop->stop_times, stop->num_stop_times, sizeof *stop->stop_times);
		stop->stop_times[stop->num_stop_times - 1] = stop_time;
	}
}

void stops_add_transfer(stops_t *stop, transfers_t *transfer)
{
	if (stop == NULL || transfer == NULL) return;
	
	#pragma omp critical(stops_add_transfer)
	{
		stop->num_transfers++;
		stop->transfers = erealloc(stop->transfers, stop->num_transfers, sizeof *stop->transfers);
		stop->transfers[stop->num_transfers - 1] = transfer;
	}
}

struct sort_stop_times_t
{
	int (*sort)(stop_times_t * const *, stop_times_t * const *);
};

static void sort_stop_times(stops_t *stop, struct sort_stop_times_t *data)
{
	qsort(stop->stop_times, stop->num_stop_times, sizeof *stop->stop_times, HT_CMP_DATA(data->sort));
}

void stops_sort_stop_times(const gtfs_database_t *db, int (*sort)(stop_times_t * const *, stop_times_t * const *))
{
	struct sort_stop_times_t data = {.sort = sort};
	
	hashtable_loopall(db->stops, HT_CB_LOOP(&sort_stop_times), &data);
}

int stops_are_family(const stops_t *stop1, const stops_t *stop2)
{
	assert(stop1 != NULL);
	assert(stop2 != NULL);
	
	if (stop1 == stop2) return 0;
	// stop1 is parent of stop2
	for (size_t i = 0; i < stop1->num_children_stations; i++)
	{
		if (stop2 == stop1->children_stations[i]) return 1;
	}
	// stop2 is parent of stop1
	for (size_t i = 0; i < stop2->num_children_stations; i++)
	{
		if (stop1 == stop2->children_stations[i]) return 1;
	}
	// stop1 and stop2 are siblings
	if (stop1->parent_station != NULL && 
		stop1->parent_station == stop2->parent_station)
		return 2;
	
	// not related
	return -1;
}

ssize_t stops_read_file(FILE *fp, gtfs_database_t *db)
{
	size_t bytes_read, retval, pos = 0;
	struct csv_parser cp;
	char buf[1024];
	
	printf("Reading STOPS...\n");
	db->stops = hashtable_init(4096);
	
	stops_csv_parser_data_t data;
	data.field = 0;
	data.row_cntr = 0;
	data.stop = NULL;
	data.db = db;
	memset(data.cols_map, 0, sizeof data.cols_map);
	
	csv_init(&cp, CSV_APPEND_NULL | CSV_EMPTY_IS_NULL);
	
	while ((bytes_read = fread(buf, 1, sizeof buf, fp)) > 0)
	{
		if ((retval = csv_parse(&cp, buf, bytes_read, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data)) != bytes_read)
		{
			if (csv_error(&cp) == CSV_EPARSE) 
			{
				printf("STOPS malformed at byte %lu\n", (unsigned long) pos + retval + 1);
				data.row_cntr = -1;
				
				goto end;
			}
			else
			{
				printf("Error while processing STOPS: %s\n", csv_strerror(csv_error(&cp)));
				data.row_cntr = -1;
				
				goto end;
			}
		}
		pos += bytes_read;
	}
	
end:
	csv_fini(&cp, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data);
	csv_free(&cp);
	
	efree(data.stop);

	hashtable_loopall(db->stops, HT_CB_LOOP(&cb_insert_ptr), db);
	
	return data.row_cntr;
}
