#ifndef _STOPS_H_
#define _STOPS_H_

#include <stdio.h>
#include <sys/types.h>

#include "types.h"
#include "hashtable.h"

typedef enum
{
	STOPS_STOP_PLATFORM = 0,
	STOPS_STATION = 1,
	STOPS_ENTRANCE_EXIT = 2,
	STOPS_GENERIC_NODE = 3,
	STOPS_BOARDING_AREA = 4
} stops_location_type_t;

typedef enum
{
	STOPS_NO_INFO_INHERIT = 0,
	STOPS_ACCESSIBLE = 1,
	STOPS_NOT_ACCESSIBLE = 2
} stops_wheelchair_boarding_t;

struct _stops_t
{
	gtfs_id_t id;
	text_t code;
	text_t name;
	size_t num_stop_times;
	stop_times_t **stop_times;
	size_t num_transfers;
	transfers_t **transfers;
	text_t desc;
	coord_t lat;
	coord_t lon;
	gtfs_id_t zone_id;	// TODO change to pointer
	url_t url;
	stops_location_type_t location_type;
	gtfs_id_t parent_station_id;
	stops_t *parent_station;
	size_t num_children_stations;
	stops_t **children_stations;
	timezone_t timezone;
	stops_wheelchair_boarding_t wheelchair_boarding;
	gtfs_id_t level_id;	// TODO change to pointer
	text_t platform_code;
};

typedef enum
{
	STOP_ID = 1,
	STOP_CODE,
	STOP_NAME,
	STOP_DESC,
	STOP_LAT,
	STOP_LON,
	STOP_ZONE_ID,
	STOP_URL,
	STOP_LOC_TYPE,
	STOP_PARENT_STATION,
	STOP_TIMEZONE,
	STOP_WHEELCHAIR_BOARDING,
	STOP_LEVEL_ID,
	STOP_PLATFORM_CODE,
	stops_fields_t_NUM_MEMBERS
} stops_fields_t;

extern const char *STOPS_FIELDS[];

void stops_print_compact(const char *title, const stops_t *stops, const char *fallback);
void stops_print(const stops_t *stops);
int stops_cmp(stops_t * const *stop1, stops_t * const *stop2);
void stops_free(stops_t **stops);
void stops_add_stop_time(stops_t *stop, stop_times_t *stop_time);
void stops_add_transfer(stops_t *stop, transfers_t *transfer);
void stops_sort_stop_times(const gtfs_database_t *db, int (*sort)(stop_times_t * const *, stop_times_t * const *));
int stops_are_family(const stops_t *needle, const stops_t *haystack);
ssize_t stops_read_file(FILE *fp, gtfs_database_t *db);

#endif //_STOPS_H_
