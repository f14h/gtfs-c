#ifndef _TERM_COLORS
#define _TERM_COLORS

#define BOLD	"\x1B[1m"
#define UNDL	"\x1B[4m"
#define BLNK	"\x1B[5m"

#define RED		"\x1B[31m"
#define GRN		"\x1B[32m"
#define YEL		"\x1B[33m"
#define BLU		"\x1B[34m"
#define MAG		"\x1B[35m"
#define CYN		"\x1B[36m"
#define WHT		"\x1B[37m"

#define BDGR	"\x1B[100m"

#define RESET	"\x1B[0m"

#endif
