#include "transfers.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <csv.h>
#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "utils.h"
#include "debug.h"

#define TID omp_get_thread_num()
#define Thds omp_get_max_threads()

const char *TRANSFERS_FIELDS[] = 
{
	"NULL",
	"from_stop_id",
	"to_stop_id",
	"transfer_type",
	"min_transfer_time",
	"from_trip_id",
	"to_trip_id",
	"from_route_id",
	"to_route_id"
};

typedef struct
{
	size_t *field;
	ssize_t row_cntr;
	transfers_t **transfer;
	const gtfs_database_t *db;
	transfers_fields_t cols_map[transfers_fields_t_NUM_MEMBERS];
} transfers_csv_parser_data_t;

void transfers_print(const transfers_t *transfers)
{
	assert(transfers != NULL);
		
	#pragma omp critical(transfers_print)
	{
		printf("--------------------\n");
		stops_print_compact("from_stop", transfers->from_stop, transfers->from_stop_id);
		stops_print_compact("to_stop", transfers->to_stop, transfers->to_stop_id);

		printf("transfer_type: %d\n", transfers->type);
		printf("min_transfer_time: %d\n", transfers->min_transfer_time);
		
		if (transfers->from_trip_id != NULL)
			trips_print_compact("from_trip", transfers->from_trip, transfers->from_trip_id);
		
		if (transfers->to_trip_id != NULL)
			trips_print_compact("to_trip", transfers->to_trip, transfers->to_trip_id);
		
		if (transfers->from_route_id != NULL)
			routes_print_compact("from_route", transfers->from_route, transfers->from_route_id);
		
		if (transfers->to_route_id != NULL)
			routes_print_compact("to_route", transfers->to_route, transfers->to_route_id);

		printf("--------------------\n");
	}
}

int transfers_cmp(transfers_t * const *transfer1, transfers_t * const *transfer2)
{
	if (*transfer1 == NULL && *transfer2 == NULL) return 0;
	if (*transfer1 == NULL) return -1;
	if (*transfer2 == NULL) return 1;
	
	if (*transfer1 == *transfer2) return 0;
	
	int r = stops_cmp(&(*transfer1)->from_stop, &(*transfer2)->to_stop);
	if (r == 0)
		r = stops_cmp(&(*transfer1)->to_stop, &(*transfer2)->to_stop);

	return r;
}

void transfers_free(transfers_t **transfers)
{
	transfers_t *transfer;
	
	assert(transfers != NULL);
	assert(*transfers != NULL);
	
	transfer = *transfers;
		
	efree(transfer->from_stop_id);
	efree(transfer->to_stop_id);
	efree(transfer->from_trip_id);
	efree(transfer->to_trip_id);
	efree(transfer->from_route_id);
	efree(transfer->to_route_id);
	efree(transfer);
	
	*transfers = NULL;
}

static void cb1(char *field, size_t size, transfers_csv_parser_data_t *p)
{
	ssize_t index;
	
	if (p->row_cntr == 0)
	{
		index = get_index(TRANSFERS_FIELDS, field, transfers_fields_t_NUM_MEMBERS);
		if (index >= 0)
		{
			p->cols_map[p->field[TID]] = (transfers_fields_t) index;
		}
		else
			fprintf(stderr, "Error reading TRANSFERS file: Unknown header <%s>\n", field);
	}
	else
	{
		switch (p->cols_map[p->field[TID]])
		{
			case TRANSFER_FROM_STOP_ID:
				p->transfer[TID]->from_stop_id = estrdup(field);
				p->transfer[TID]->from_stop = hashtable_get_data(p->db->stops, field);
				stops_add_transfer(p->transfer[TID]->from_stop, p->transfer[TID]);
				break;
				
			case TRANSFER_TO_STOP_ID:
				p->transfer[TID]->to_stop_id = estrdup(field);
				p->transfer[TID]->to_stop = hashtable_get_data(p->db->stops, field);
				stops_add_transfer(p->transfer[TID]->to_stop, p->transfer[TID]);
				break;
				
			case TRANSFER_TYPE:
				p->transfer[TID]->type = (unsigned int) estrtol(field, NULL, 10, TRANSFERS_RECOMMENDED);
				break;
			
			case TRANSFER_MIN_TRANSFER_TIME:
				p->transfer[TID]->min_transfer_time = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case TRANSFER_FROM_TRIP_ID:
				p->transfer[TID]->from_trip_id = estrdup(field);
				p->transfer[TID]->from_trip = hashtable_get_data(p->db->trips, field);
				trips_add_transfer(p->transfer[TID]->from_trip, p->transfer[TID]);
				break;
				
			case TRANSFER_TO_TRIP_ID:
				p->transfer[TID]->to_trip_id = estrdup(field);
				p->transfer[TID]->to_trip = hashtable_get_data(p->db->trips, field);
				trips_add_transfer(p->transfer[TID]->to_trip, p->transfer[TID]);
				break;
				
			case TRANSFER_FROM_ROUTE_ID:
				p->transfer[TID]->from_route_id = estrdup(field);
				p->transfer[TID]->from_route = hashtable_get_data(p->db->routes, field);
				routes_add_transfer(p->transfer[TID]->from_route, p->transfer[TID]);
				break;
				
			case TRANSFER_TO_ROUTE_ID:
				p->transfer[TID]->to_route_id = estrdup(field);
				p->transfer[TID]->to_route = hashtable_get_data(p->db->routes, field);
				routes_add_transfer(p->transfer[TID]->to_route, p->transfer[TID]);
				break;
				
			default:
				fprintf(stderr, "Error creating TRANSFERS: Unknown field %d\n", p->cols_map[p->field[TID]]);
				break;
		}
	}
	
	p->field[TID]++;
}

static void cb2(int chr, transfers_csv_parser_data_t *p)
{
	char buf[128];
	
	if (p->row_cntr > 0)
	{
		snprintf(buf, sizeof buf, "%s%s", p->transfer[TID]->from_stop_id, p->transfer[TID]->to_stop_id);
		STRNCAT_INN(buf, p->transfer[TID]->from_trip_id, sizeof buf);
		STRNCAT_INN(buf, p->transfer[TID]->to_trip_id, sizeof buf);
		STRNCAT_INN(buf, p->transfer[TID]->from_route_id, sizeof buf);
		STRNCAT_INN(buf, p->transfer[TID]->to_route_id, sizeof buf);
		
		hashtable_set
		(
			p->db->transfers,
			buf,
			p->transfer[TID],
			HT_CMP_DATA(&transfers_cmp),
			HT_PRINT_DATA(&transfers_print),
			HT_FREE_DATA(&transfers_free)
		);
	}
	else
	{
		for (int i = 1; i < omp_get_num_threads(); i++)
		{
			p->transfer[i] = ecalloc(1, sizeof *p->transfer[i]);
		}
	}
	
	p->field[TID] = 0;
	
	#pragma omp atomic update
	p->row_cntr++;
	
	p->transfer[TID] = ecalloc(1, sizeof *p->transfer[TID]);
}

ssize_t transfers_read_file(FILE *fp, gtfs_database_t *db)
{
	size_t bytes_read, retval, pos = 0;
	struct csv_parser cp;
	char *res;
	char buf[1024];
	
	printf("Reading TRANSFERS...\n");
	db->transfers = hashtable_init(32768);
	
	transfers_csv_parser_data_t data;
	data.field = ecalloc((size_t) Thds, sizeof *data.field);
	data.row_cntr = 0;
	data.transfer = ecalloc((size_t) Thds, sizeof *data.transfer);
	data.db = db;
	memset(data.cols_map, 0, sizeof data.cols_map);
	
	#pragma omp parallel private(buf, cp, res, bytes_read, retval)
	{
	bytes_read = 0;
	csv_init(&cp, CSV_APPEND_NULL | CSV_EMPTY_IS_NULL);
	
	while (1)
	{
		while (TID != 0 && data.row_cntr == 0)
		;
		
		#pragma omp critical(transfers_read_line)
		{
			pos += bytes_read;
			res = fgets(buf, sizeof buf, fp);
		}
		if (res == NULL) break;
		bytes_read = strlen(buf);
		
		if ((retval = csv_parse(&cp, buf, bytes_read, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data)) != bytes_read)
		{
			if (csv_error(&cp) == CSV_EPARSE) 
			{
				printf("TRANSFERS malformed at byte %lu\n", (unsigned long) pos + retval + 1);
				
				break;
			}
			else
			{
				printf("Error while processing TRANSFERS: %s\n", csv_strerror(csv_error(&cp)));
				
				break;
			}
		}
	}
	
	csv_fini(&cp, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data);
	csv_free(&cp);

	efree(data.transfer[TID]);
	}
	
	efree(data.transfer);
	efree(data.field);
	
	return data.row_cntr;
}
