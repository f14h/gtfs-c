#ifndef _TRANSFERS_H_
#define _TRANSFERS_H_

#include <stdio.h>
#include <sys/types.h>

#include "types.h"
#include "hashtable.h"

#include "routes.h"
#include "stops.h"
#include "trips.h"

typedef enum
{
	TRANSFERS_RECOMMENDED = 0,
	TRANSFERS_TIMED = 1,
	TRANSFERS_TIME_NEEDED = 2,
	TRANSFERS_NOT_POSSIBLE = 3
} transfers_type_t;

struct _transfers_t
{
	gtfs_id_t from_stop_id;
	stops_t *from_stop;
	gtfs_id_t to_stop_id;
	stops_t *to_stop;
	transfers_type_t type;
	unsigned int min_transfer_time;
	gtfs_id_t from_trip_id;
	trips_t *from_trip;
	gtfs_id_t to_trip_id;
	trips_t *to_trip;
	gtfs_id_t from_route_id;
	routes_t *from_route;
	gtfs_id_t to_route_id;
	routes_t *to_route;
};

typedef enum
{
	TRANSFER_FROM_STOP_ID = 1,
	TRANSFER_TO_STOP_ID,
	TRANSFER_TYPE,
	TRANSFER_MIN_TRANSFER_TIME,
	TRANSFER_FROM_TRIP_ID,
	TRANSFER_TO_TRIP_ID,
	TRANSFER_FROM_ROUTE_ID,
	TRANSFER_TO_ROUTE_ID,
	transfers_fields_t_NUM_MEMBERS
} transfers_fields_t;

extern const char *TRANSFERS_FIELDS[];

void transfers_print(const transfers_t *transfers);
int transfers_cmp(transfers_t * const *transfer1, transfers_t * const *transfer2);
void transfers_free(transfers_t **transfers);
ssize_t transfers_read_file(FILE *fp, gtfs_database_t *db);

#endif //_TRANSFERS_H_
