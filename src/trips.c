#include "trips.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <csv.h>
#ifdef _OPENMP
	#include <omp.h>     // for #pragma omp
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
	#define omp_get_max_threads() 1
#endif

#include "utils.h"
#include "debug.h"

const char *TRIPS_FIELDS[] = 
{
	"NULL",
	"route_id",
	"service_id",
	"trip_id",
	"trip_headsign",
	"trip_short_name",
	"direction_id",
	"block_id",
	"shape_id",
	"wheelchair_accessible",
	"bikes_allowed"
};

typedef struct
{
	size_t field;
	ssize_t row_cntr;
	trips_t *trip;
	const gtfs_database_t *db;
	trips_fields_t cols_map[trips_fields_t_NUM_MEMBERS];
} trips_csv_parser_data_t;

void trips_print_compact(const char *title, const trips_t *trips, const char *fallback)
{
	assert(title != NULL);
	assert(fallback != NULL);
	
	if (trips == NULL)
		printf("%s: %s\n", title, fallback);
	else
	{
		if (trips->route != NULL)
		{
			printf("%s: %s --> %s (%s)\n", title, trips->route->short_name != NULL ? trips->route->short_name : trips->route->long_name, trips->headsign, trips->id);
		}
		else
		{
			printf("%s: ??? --> %s\n", title, trips->headsign);
		}
	}
}

void trips_print(const trips_t *trips)
{
	#pragma omp critical(trips_print)
	{
		assert(trips != NULL);
		
		printf("--------------------\n");
		routes_print_compact("route", trips->route, trips->route_id);
		calendar_print_compact("service", trips->service, trips->service_id);
		printf("num_stop_times: %zu\n", trips->num_stop_times);
		printf("trip_id: %s\n", trips->id);
		printf("trip_headsign: %s\n", trips->headsign);
		printf("trip_short_name: %s\n", trips->short_name);
		printf("direction_id: %d\n", trips->direction_id);
		printf("block_id: %s\n", trips->block_id);
		printf("shape_id: %s\n", trips->shape_id);
		printf("wheelchair_accessible: %d\n", trips->wheelchair_accessible);
		printf("bikes_allowed: %d\n", trips->bikes_allowed);
		printf("--------------------\n");
	}
}

int trips_cmp(trips_t * const *trip1, trips_t * const *trip2)
{
	if (*trip1 == NULL && *trip2 == NULL) return 0;
	if (*trip1 == NULL) return -1;
	if (*trip2 == NULL) return 1;
	
	if (*trip1 == *trip2) return 0;
	
	int r = routes_cmp(&(*trip1)->route, &(*trip2)->route);
	if (r == 0)
	{
		if ((*trip1)->headsign != NULL && (*trip2)->headsign != NULL)
			r = strcmp((*trip1)->headsign, (*trip2)->headsign);
		
		if (r == 0)
			r = calendar_cmp(&(*trip1)->service, &(*trip2)->service);
	}
	
	return r;
}

void trips_free(trips_t **trips)
{
	trips_t *trip;
	
	assert(trips != NULL);
	assert(*trips != NULL);
	
	trip = *trips;
		
	efree(trip->route_id);
	efree(trip->service_id);
	efree(trip->id);
	efree(trip->headsign);
	efree(trip->short_name);
	efree(trip->block_id);
	efree(trip->shape_id);
	efree(trip->stop_times);
	efree(trip->transfers);
	efree(trip);
	
	*trips = NULL;
}

static void cb1(char *field, size_t size, trips_csv_parser_data_t *p)
{
	ssize_t index;
	
	if (p->row_cntr == 0)
	{
		index = get_index(TRIPS_FIELDS, field, trips_fields_t_NUM_MEMBERS);
		if (index >= 0)
		{
			p->cols_map[p->field] = (trips_fields_t) index;
		}
		else
			fprintf(stderr, "Error reading TRIPS file: Unknown header <%s>\n", field);
	}
	else
	{
		switch (p->cols_map[p->field])
		{
			case TRIP_ROUTE_ID:
				p->trip->route_id = estrdup(field);
				p->trip->route = hashtable_get_data(p->db->routes, p->trip->route_id);
				break;
				
			case TRIP_SERVICE_ID:
				p->trip->service_id = estrdup(field);
				p->trip->service = hashtable_get_data(p->db->calendar, p->trip->service_id);
				break;
				
			case TRIP_ID:
				p->trip->id = estrdup(field);
				break;
				
			case TRIP_HEADSIGN:
				p->trip->headsign = estrdup(field);
				break;
				
			case TRIP_SHORT_NAME:
				p->trip->short_name = estrdup(field);
				break;
				
			case TRIP_DIRECTION_ID:
				p->trip->direction_id = (unsigned int) estrtol(field, NULL, 10, 0);
				break;
				
			case TRIP_BLOCK_ID:
				p->trip->block_id = estrdup(field);
				break;
				
			case TRIP_SHAPE_ID:
				p->trip->shape_id = estrdup(field);
				break;
				
			case TRIP_WHEELCHAIR_ACCESSIBLE:
				p->trip->wheelchair_accessible = (unsigned int) estrtol(field, NULL, 10, TRIPS_BIKE_NO_INFO);
				break;
				
			case TRIP_BIKES_ALLOWED:
				p->trip->bikes_allowed = (unsigned int) estrtol(field, NULL, 10, TRIPS_WHEELCHAIR_NO_INFO);
				break;
				
			default:
				fprintf(stderr, "Error creating TRIP: Unknown field %d\n", p->cols_map[p->field]);
				break;
		}
	}
	
	p->field++;
}

static void cb2(int chr, trips_csv_parser_data_t *p)
{
	if (p->row_cntr > 0)
		hashtable_set
		(
			p->db->trips,
			p->trip->id,
			p->trip,
			HT_CMP_DATA(&trips_cmp),
			HT_PRINT_DATA(&trips_print),
			HT_FREE_DATA(&trips_free)
		);	
	
	p->field = 0;
	p->row_cntr++;
	
	p->trip = ecalloc(1, sizeof *p->trip);;
}

void trips_add_stop_time(trips_t *trip, stop_times_t *stop_time)
{
	if (trip == NULL || stop_time == NULL) return;
	
	#pragma omp critical(trips_add_stop_time)
	{
		trip->num_stop_times++;
		trip->stop_times = erealloc(trip->stop_times, trip->num_stop_times, sizeof *trip->stop_times);
		trip->stop_times[trip->num_stop_times - 1] = stop_time;
	}
}

void trips_add_transfer(trips_t *trip, transfers_t *transfer)
{
	if (trip == NULL || transfer == NULL) return;
	
	#pragma omp critical(trips_add_transfer)
	{
		trip->num_transfers++;
		trip->transfers = erealloc(trip->transfers, trip->num_transfers, sizeof *trip->transfers);
		trip->transfers[trip->num_transfers - 1] = transfer;
	}
}

struct sort_stop_times_t
{
	int (*sort)(stop_times_t * const *, stop_times_t * const *);
};

static void sort_stop_times(trips_t *trip, struct sort_stop_times_t *data)
{
	qsort(trip->stop_times, trip->num_stop_times, sizeof *trip->stop_times, HT_CMP_DATA(data->sort));
}

void trips_sort_stop_times(const gtfs_database_t *db, int (*sort)(stop_times_t * const *, stop_times_t * const *))
{
	struct sort_stop_times_t data = {.sort = sort};
	
	hashtable_loopall(db->trips, HT_CB_LOOP(&sort_stop_times), &data);
}

ssize_t trips_read_file(FILE *fp, gtfs_database_t *db)
{
	size_t bytes_read, retval, pos = 0;
	struct csv_parser cp;
	char buf[1024];
	
	printf("Reading TRIPS...\n");
	db->trips = hashtable_init(4096);
	
	trips_csv_parser_data_t data;
	data.field = 0;
	data.row_cntr = 0;
	data.trip = NULL;
	data.db = db;
	memset(data.cols_map, 0, sizeof data.cols_map);
	
	csv_init(&cp, CSV_APPEND_NULL | CSV_EMPTY_IS_NULL);
	
	while ((bytes_read = fread(buf, 1, sizeof buf, fp)) > 0)
	{
		if ((retval = csv_parse(&cp, buf, bytes_read, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data)) != bytes_read)
		{
			if (csv_error(&cp) == CSV_EPARSE) 
			{
				printf("TRIPS malformed at byte %lu\n", (unsigned long) pos + retval + 1);
				data.row_cntr = -1;
				
				goto end;
			}
			else
			{
				printf("Error while processing TRIPS: %s\n", csv_strerror(csv_error(&cp)));
				data.row_cntr = -1;
				
				goto end;
			}
		}
		pos += bytes_read;
	}
	
end:
	csv_fini(&cp, LIBCSV_CB1(cb1), LIBCSV_CB2(cb2), &data);
	csv_free(&cp);

	efree(data.trip);

	return data.row_cntr;
}
