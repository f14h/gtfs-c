#ifndef _TRIPS_H_
#define _TRIPS_H_

#include <stdio.h>
#include <sys/types.h>

#include "types.h"
#include "hashtable.h"

#include "routes.h"
#include "calendar.h"

typedef enum
{
	TRIPS_ONE_DIRECTION = 0,
	TRIPS_OPPOSITE_DIRECTION = 1
} trips_direction_t;

typedef enum
{
	TRIPS_WHEELCHAIR_NO_INFO = 0,
	TRIPS_WHEELCHAIR_ACCESSIBLE = 1,
	TRIPS_WHEELCHAIR_NOT_ACCESSIBLE = 2
} stops_wheelchair_accessible_t;

typedef enum
{
	TRIPS_BIKE_NO_INFO = 0,
	TRIPS_BIKE_ALLOWED = 1,
	TRIPS_BIKE_NOT_ALLOWED = 2
} stops_bikes_allowed_t;

struct _trips_t
{
	gtfs_id_t route_id;
	routes_t *route;
	gtfs_id_t service_id;
	calendar_t *service;
	size_t num_stop_times;
	stop_times_t **stop_times;
	size_t num_transfers;
	transfers_t **transfers;
	gtfs_id_t id;
	text_t headsign;
	text_t short_name;
	trips_direction_t direction_id;
	gtfs_id_t block_id;
	gtfs_id_t shape_id;
//	shape_t shape[];
	stops_wheelchair_accessible_t wheelchair_accessible;
	stops_bikes_allowed_t bikes_allowed;
};

typedef enum
{
	TRIP_ROUTE_ID = 1,
	TRIP_SERVICE_ID,
	TRIP_ID,
	TRIP_HEADSIGN,
	TRIP_SHORT_NAME,
	TRIP_DIRECTION_ID,
	TRIP_BLOCK_ID,
	TRIP_SHAPE_ID,
	TRIP_WHEELCHAIR_ACCESSIBLE,
	TRIP_BIKES_ALLOWED,
	trips_fields_t_NUM_MEMBERS
} trips_fields_t;

extern const char *TRIPS_FIELDS[];

void trips_print_compact(const char *title, const trips_t *trips, const char *fallback);
void trips_print(const trips_t *trips);
int trips_cmp(trips_t * const *trip1, trips_t * const *trip2);
void trips_free(trips_t **trips);
void trips_add_stop_time(trips_t *trip, stop_times_t *stop_time);
void trips_add_transfer(trips_t *trip, transfers_t *transfer);
void trips_sort_stop_times(const gtfs_database_t *db, int (*sort)(stop_times_t * const *, stop_times_t * const *));
ssize_t trips_read_file(FILE *fp, gtfs_database_t *db);

#endif //_TRIPS_H_
