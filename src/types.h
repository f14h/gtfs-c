#ifndef _TYPES_H_
#define _TYPES_H_

#include <stdint.h>
#include <string.h>

#include <time.h>

#include "hashtable.h"
#include "utils.h"

typedef uint32_t color_t;
typedef char * currency_code_t;
typedef time_t date_time_t;
typedef char * email_t;
typedef char * gtfs_id_t;
typedef char * lang_code_t;
typedef float coord_t;
typedef char * phone_t;
typedef char * text_t;
typedef char * timezone_t;
typedef char * url_t;

typedef struct _trips_t trips_t;
typedef struct _agency_t agency_t;
typedef struct _stop_times_t stop_times_t;
typedef struct _routes_t routes_t;
typedef struct _stops_t stops_t;
typedef struct _calendar_dates_t calendar_dates_t;
typedef struct _calendar_t calendar_t;
typedef struct _transfers_t transfers_t;

typedef struct _gtfs_database_t
{
	hashtable_t *agency;
	hashtable_t *stops;
	hashtable_t *routes;
	hashtable_t *trips;
	hashtable_t *stop_times;
	hashtable_t *calendar;
	hashtable_t *fare_attributes;
	hashtable_t *fare_rules;
	hashtable_t *shapes;
	hashtable_t *frequencies;
	hashtable_t *transfers;
	hashtable_t *pathways;
	hashtable_t *levels;
	hashtable_t *feed_info;
	hashtable_t *translations;
	hashtable_t *attributions;
} gtfs_database_t;

static inline date_time_t str_to_date_time_t(const char *fmt, const char *str)
{
	struct tm tm;
	memset(&tm, 0, sizeof tm);
	tm.tm_isdst = -1; // let mktime figure out if dst or not
	
	strptime(str, fmt, &tm);
	return mktime(&tm);
}

static inline const char *date_time_t_to_str(const char *fmt, const date_time_t time)
{
	static char str[32];
	
	strftime(str, sizeof str, fmt, localtime(&time));
	return str;
}

static inline date_time_t date_time_t_add_sec(const date_time_t time, const uint32_t sec)
{
	struct tm tm;
	memset(&tm, 0, sizeof tm);
	tm.tm_isdst = -1; // let localtime figure out if dst or not
	
	tm = *localtime_r(&time, &tm);
	tm.tm_sec += sec;
	
	return mktime(&tm);
}

static inline date_time_t date_time_t_sub_sec(const date_time_t time, const uint32_t sec)
{
	struct tm tm;
	memset(&tm, 0, sizeof tm);
	tm.tm_isdst = -1; // let localtime figure out if dst or not
	
	tm = *localtime_r(&time, &tm);
	tm.tm_sec -= sec;
	
	return mktime(&tm);
}

static inline int date_time_t_cmp(const date_time_t time1, const date_time_t time2)
{
	return INTCMP(time1, time2);
}

#endif //_TYPES_H_
