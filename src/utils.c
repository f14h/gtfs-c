#include "utils.h"

#include <fcntl.h>   // for open, SEEK_END, SEEK_SET
#include <stdio.h>   // for perror, size_t, FILE, NULL, clearerr, ferror
#include <stdlib.h>  // for exit, EXIT_FAILURE, calloc, malloc, realloc
#include <string.h>  // for strdup, strndup
#include <unistd.h>  // for close, write
#include <errno.h>
#include <limits.h>
#include <libexplain/fread.h>
#include <libexplain/fwrite.h>

void *emalloc(size_t nmemb, size_t size)
{
	if (size == 0 || nmemb == 0) return NULL;
	
	size_t s = nmemb * size;
	if (s / nmemb != size)
	{
		errno = EOVERFLOW;
		#ifndef TEST
		perror("malloc failed");
		abort();
		#endif
		
		return NULL;
	}
	
	void *p = malloc(s);
	if (p == NULL)
	{
		#ifndef TEST
		perror("malloc failed");
		abort();
		#endif
	}
	
	return p;
}

void *ecalloc(size_t nmemb, size_t size)
{
	if (size == 0 || nmemb == 0) return NULL;
	
	void *p = calloc(nmemb, size);
	if (p == NULL)
	{
		#ifndef TEST
		perror("calloc failed");
		abort();
		#endif
	}
	
	return p;
}

void *erealloc(void *ptr, size_t nmemb, size_t size)
{
	if (size == 0 || nmemb == 0) return NULL;
	
	size_t s = nmemb * size;
	if (s / nmemb != size)
	{
		errno = EOVERFLOW;
		#ifndef TEST
		perror("realloc failed");
		abort();
		#endif
	}
	
	void *p = realloc(ptr, s);
	if (p == NULL)
	{
		#ifndef TEST
		perror("realloc failed");
		abort();
		#endif
	}
	
	return p;
}

char *estrndup(const char *s, size_t n)
{
	if (s == NULL) return NULL;
	
	char *p = strndup(s, n);
	if (p == NULL)
	{
		#ifndef TEST
		perror("'strndup' failed");
		abort();
		#endif
	}
	
	return p;
}

char *estrdup(const char *s)
{
	if (s == NULL) return NULL;
	
	char *p = strdup(s);
	if (p == NULL)
	{
		#ifndef TEST
		perror("'strdup' failed");
		abort();
		#endif
	}
	
	return p;
}

FILE *efopen(const char *filename, const char *flags)
{
	FILE *file = fopen(filename, flags);
	if (file == NULL)
	{
		#ifndef TEST
		fprintf(stderr, "<%s>: ", filename);
		perror("error opening file");
		
		abort();
		#endif
	}
	
	return file;
}

void efclose(FILE *fp)
{
	if (fp == NULL)
	{
		errno = EFAULT;
		#ifndef TEST
		perror("'fclose' failed");
		abort();
		#else
		return;
		#endif
	}
	
	if (fclose(fp) < 0)
	{
		#ifndef TEST
		perror("'fclose' failed");
		abort();
		#endif
	}
}

size_t efwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	size_t r = fwrite(ptr, size, nmemb, stream);
	if (r == 0 && ferror(stream))
	{
		#ifndef TEST
		fprintf(stderr, "%s\n", explain_fwrite(ptr, size, nmemb, stream));
		
		abort();
		#else
		return 0;
		#endif
	}

	return r;
}

size_t efread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	memset(ptr, 0, size * nmemb);
	size_t r = fread(ptr, size, nmemb, stream);
	if (r == 0 && ferror(stream))
	{
		#ifndef TEST
		fprintf(stderr, "%s\n", explain_fread(ptr, size, nmemb, stream));
		
		abort();
		#else
		return 0;
		#endif
	}

	return r;
}

int efseek(FILE *stream, long offset, int whence)
{
	int r = fseek(stream, offset, whence);
	if (r < 0)
	{
		#ifndef TEST
		perror("'fseek' failed");
		abort();
		#endif
	}
	
	return r;
}

long eftell(FILE *stream)
{
	long r = ftell(stream);
	if (r < 0)
	{
		#ifndef TEST
		perror("'ftell' failed");
		abort();
		#endif
	}
	
	return r;
}

size_t write_to_file(const void *ptr, size_t size, size_t nmemb, const char *filename)
{
	FILE *file;
	size_t r;
	
	file = efopen(filename, "w");
	r = efwrite(ptr, size, nmemb, file);
	
	efclose(file);
	
	return r;
}

size_t read_from_file(char **ptr, const char *filename)
{
	FILE *file;
	size_t r, size;
	
	file = efopen(filename, "r");
	
	efseek(file, 0, SEEK_END);
	size = (size_t) eftell(file);
	efseek(file, 0, SEEK_SET);
	
	*ptr = emalloc(size, sizeof **ptr);
	r = efread(*ptr, 1, size, file);
	
	efclose(file);
	
	return r;
}

void eclose(int fd)
{
	if (close(fd) < 0)
	{
		#ifndef TEST
		perror("'close' failed");
		abort();
		#endif
	}
}

int eopen(const char *pathname, int flags)
{
	int fd = open(pathname, flags);
	
	if (fd < 0)
	{
		#ifndef TEST
		perror("'open' failed");
		abort();
		#endif
	}
	
	return fd;
}

ssize_t ewrite(int fd, const void *buf, size_t count)
{
	ssize_t c;
	if ((c = write(fd, buf, count)) < 0)
	{
		#ifndef TEST
		perror("'write' failed");
		abort();
		#endif
	}
	
	return c;
}

DIR *eopendir(const char *name)
{
	DIR *d = opendir(name);
	if (d == NULL)
	{
		#ifndef TEST
		perror("'opendir' failed");
		abort();
		#endif
	}
	
	return d;
}

void eclosedir(DIR *dirp)
{
	if (closedir(dirp) < 0)
	{
		#ifndef TEST
		perror("'closedir' failed");
		abort();
		#endif
	}
}

long int estrtol(const char *nptr, char **endptr, int base, long int def)
{
	long int res;
	char *eendptr;

	if (nptr == NULL) return def;
	
	errno = 0;
	res = strtol(nptr, &eendptr, base);
	if ((errno == ERANGE && (res == LONG_MAX || res == LONG_MIN)) || (errno != 0 && res == 0)) {
		#ifndef TEST
		perror("'strtol' failed");
		abort();
		#else
		return def;
		#endif
	}
	
	if (eendptr == nptr) res = def;
	if (endptr != NULL) *endptr = eendptr;
	
	return res;
}

// -----------------------------------------------------

ssize_t get_index(const char *haystack[], const char *needle, size_t len)
{
	ssize_t ret = -1;
	
	for (size_t i = 0; i < len; i++)
	{
		if (strcmp(haystack[i], needle) == 0)
		{
			return (ssize_t) i;
		}
	}
	
	return ret;
}
