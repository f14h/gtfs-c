#ifndef UTILS_H_
#define UTILS_H_


#include <stdio.h>   	// for perror, NULL, FILE, fopen, printf
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>

#define VISIBILITY_DEFAULT 	__attribute__ ((visibility ("default")))
#define UNUSED				__attribute__ ((unused))

#ifndef MAX
#define MAX(a, b) \
   (__extension__({ __typeof__ (a) _a = (a); \
	   __typeof__ (b) _b = (b); \
	 _a > _b ? _a : _b; }))
#endif

#ifndef MIN   
#define MIN(a, b) \
   (__extension__({ __typeof__ (a) _a = (a); \
	   __typeof__ (b) _b = (b); \
	 _a < _b ? _a : _b; }))
#endif

#ifndef INTCMP
#define INTCMP(a, b) (((a) > (b)) - ((a) < (b)))
#endif

#define CHECK_BIT(var,pos) !!((var) & (1 << (pos)))
#define SET_BIT(p,n) ((p) |= (1 << (n)))
#define CLR_BIT(p,n) ((p) &= ~(1 << (n)))

//https://stackoverflow.com/a/3553321
#define member_size(type, member) (sizeof(((type *)0)->member))
#define member_size_arr(type, member) (sizeof(((type *)0)->member) / sizeof(((type *)0)->member[0]))

#define efree(obj) ({ \
	free((obj));	\
	(obj) = NULL;	\
})

#define LIBCSV_CB1(fkt) ((void (*)(void *, size_t, void *)) (fkt))
#define LIBCSV_CB2(fkt) ((void (*)(int, void *)) (fkt))

#define STRNCAT_INN(dest, src, n) ({	\
	if ((src) != NULL)	\
		strncat((dest), (src), (n) - strlen((dest)) - 1U);	\
})

void *emalloc(size_t nmemb, size_t size);
void *ecalloc(size_t nmemb, size_t size);
void *erealloc(void *ptr, size_t nmemb, size_t size);
char *estrndup(const char *s, size_t n);
char *estrdup(const char *s);

FILE *efopen(const char *filename, const char *flags);
void efclose(FILE *fp);
size_t efwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
size_t efread(void *ptr, size_t size, size_t nmemb, FILE *stream);
int efseek(FILE *stream, long offset, int whence);
long eftell(FILE *stream);
size_t write_to_file(const void *ptr, size_t size, size_t nmemb, const char *filename);
size_t read_from_file(char **ptr, const char *filename);

void eclose(int fd);
int eopen(const char *pathname, int flags);
ssize_t ewrite(int fd, const void *buf, size_t count);

DIR *eopendir(const char *name);
void eclosedir(DIR *dirp);

long int estrtol(const char *nptr, char **endptr, int base, long int def);

ssize_t get_index(const char *haystack[], const char *needle, size_t len);

static inline void efree_wrapper(void **p)
{
	efree(*p);
}

#endif // UTILS_H_
