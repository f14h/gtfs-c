#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#define CUTEST(test) ({ \
	CU_pTest ret = (test);	\
	if (ret == NULL) {	\
		CU_cleanup_registry();	\
		return CU_get_error() != CUE_SUCCESS;	\
	}	\
})

#define CUSUITE(suite) ({ \
	CU_pSuite ret = (suite);	\
	if (ret == NULL) {	\
		CU_cleanup_registry();	\
		return CU_get_error() != CUE_SUCCESS;	\
	}	\
	ret;	\
})

#define CU_ASSERT_MALLOC(assert, ptr, ...) ({	\
	void *p = (ptr);	\
	assert(p, ## __VA_ARGS__);	\
	free(p);	\
})

#include "test_utils.c"
#include "test_hashtable.c"
#include "test_agency.c"

int main(void)
{
	CU_pSuite s_utils, s_hashtable, s_agency;

	/* initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry())
	return CU_get_error() != CUE_SUCCESS;

	/* add suites to the registry */
	s_utils = CUSUITE(CU_add_suite("utils", init_utils, clean_utils));
	s_hashtable = CUSUITE(CU_add_suite("hashtable", init_hashtable, clean_hashtable));
	s_agency = CUSUITE(CU_add_suite("agency", init_agency, clean_agency));

	/* add the tests to the suite */
	CUTEST(CU_ADD_TEST(s_utils, test_emalloc));
	CUTEST(CU_ADD_TEST(s_utils, test_ecalloc));
	CUTEST(CU_ADD_TEST(s_utils, test_erealloc));
	CUTEST(CU_ADD_TEST(s_utils, test_estrndup));
	CUTEST(CU_ADD_TEST(s_utils, test_estrdup));
	CUTEST(CU_ADD_TEST(s_utils, test_efopen));
	CUTEST(CU_ADD_TEST(s_utils, test_efclose));
	CUTEST(CU_ADD_TEST(s_utils, test_efread));
	CUTEST(CU_ADD_TEST(s_utils, test_efwrite));
	CUTEST(CU_ADD_TEST(s_utils, test_estrtol));
	
	CUTEST(CU_ADD_TEST(s_hashtable, test_init));
	CUTEST(CU_ADD_TEST(s_hashtable, test_free));
	CUTEST(CU_ADD_TEST(s_hashtable, test_set_get_simple));
	CUTEST(CU_ADD_TEST(s_hashtable, test_set_get_list));
	CUTEST(CU_ADD_TEST(s_hashtable, test_loop));
	
	CUTEST(CU_ADD_TEST(s_agency, test_agency_print_free));
	
	/* Run all tests using the CUnit Basic interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	
	unsigned int res = CU_get_number_of_tests_failed();
	
	CU_cleanup_registry();
	
	return res != 0;
}
