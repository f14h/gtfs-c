#include "../src/agency.h"

#include <CUnit/CUnit.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

int init_agency(void)
{
	
	return 0;
}

int clean_agency(void)
{
	
	return 0;
}

void test_agency_print_free(void)
{
	agency_t *ag = emalloc(1, sizeof *ag);
	
	ag->id = estrdup("123");
	ag->name = estrdup("Test agency");
	ag->url = estrdup("https://example.com");
	ag->timezone = estrdup("Europe/Berlin");
	ag->lang = estrdup("de");
	ag->phone = estrdup("0123456789");
	ag->fare_url = estrdup("https://example.com");
	ag->email = estrdup("test@example.com");
	
	agency_print(ag);
	agency_free(&ag);
	
	CU_ASSERT_PTR_NULL(ag);
}
