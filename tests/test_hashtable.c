#include "../src/hashtable.h"

#include <CUnit/CUnit.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

hashtable_t *tbl2, *tbl3;

int init_hashtable(void)
{
	tbl2 = hashtable_init(256);
	tbl3 = hashtable_init(2);
	
	return 0;
}

int clean_hashtable(void)
{
	hashtable_free(&tbl2);
	hashtable_free(&tbl3);
	
	return 0;
}

static void free_mapper(void **p)
{
	efree(*p);
}

static void print_mapper(const void *p)
{
	const char *s = (const char *) p;
	printf("%s\n", s);
}

static int cmp_mapper(const void *v1, const void *v2)
{
	const char *s1 = *((char * const *) v1);
	const char *s2 = *((char * const *) v2);
	
	return strcmp(s1, s2);
}

void test_init(void)
{
	hashtable_t *tbl = hashtable_init(16);
	CU_ASSERT_PTR_NOT_NULL(tbl);
	CU_ASSERT_EQUAL(tbl->capacity, 16);
	
	for (size_t i = 0; i < 16; i++)
	{
		CU_ASSERT_PTR_NULL(tbl->table[i]);
	}
	
	hashtable_free(&tbl);
}

void test_free(void)
{
	hashtable_t *tbl1 = hashtable_init(20);
	hashtable_free(&tbl1);
	CU_ASSERT_PTR_NULL(tbl1);
	
	#ifdef NDEBUG
	hashtable_free(NULL);
	hashtable_t *t = NULL;
	hashtable_free(&t);
	#endif
}

void test_set_get_simple(void)
{
	hashtable_set(tbl2, "test123", estrdup("value1"), &cmp_mapper, &print_mapper, &free_mapper);
	hashtable_set(tbl2, "hello", estrdup("value2"), &cmp_mapper, &print_mapper, &free_mapper);
	hashtable_set(tbl2, "blub", estrdup("value3"), &cmp_mapper, &print_mapper, &free_mapper);
	
	CU_ASSERT_STRING_EQUAL(hashtable_get(tbl2, "test123")->data, "value1");
	CU_ASSERT_STRING_EQUAL(hashtable_get(tbl2, "hello")->data, "value2");
	CU_ASSERT_STRING_EQUAL(hashtable_get(tbl2, "blub")->data, "value3");
	
	CU_ASSERT_PTR_NULL(hashtable_get(tbl2, "bleb"));
	CU_ASSERT_PTR_NULL(hashtable_get(tbl2, "horst"));
}

void test_set_get_list(void)
{
	hashtable_set(tbl3, "test123", estrdup("value1"), &cmp_mapper, &print_mapper, &free_mapper);
	hashtable_set(tbl3, "hello", estrdup("value2"), &cmp_mapper, &print_mapper, &free_mapper);
	hashtable_set(tbl3, "blub", estrdup("value3"), &cmp_mapper, &print_mapper, &free_mapper);
	hashtable_set(tbl3, "blab", estrdup("value4"), &cmp_mapper, &print_mapper, &free_mapper);
	
	CU_ASSERT_STRING_EQUAL(hashtable_get(tbl3, "test123")->data, "value1");
	CU_ASSERT_STRING_EQUAL(hashtable_get(tbl3, "hello")->data, "value2");
	CU_ASSERT_STRING_EQUAL(hashtable_get(tbl3, "blub")->data, "value3");
	CU_ASSERT_STRING_EQUAL(hashtable_get(tbl3, "blab")->data, "value4");
}

static void check(char *val, char *data)
{
	CU_ASSERT_NSTRING_EQUAL(val, "value", 5);
	CU_ASSERT_STRING_EQUAL(data, "data");
}

void test_loop(void)
{
	hashtable_loopall(tbl2, (void (*)(void *, const void *)) &check, "data");
	hashtable_loopall(tbl3, (void (*)(void *, const void *)) &check, "data");
}
