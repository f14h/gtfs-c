#include "../src/utils.h"

#include <CUnit/CUnit.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

int init_utils(void)
{
	const char *s = "read test\n";
	FILE *fp = fopen("testfile.txt" ,"a");
	fwrite(s, sizeof *s, strlen(s), fp);
	fclose(fp);
	
	return 0;
}

int clean_utils(void)
{
	unlink("testfile.txt");
	
	return 0;
}

void test_emalloc(void)
{
	errno = 0;
	CU_ASSERT_PTR_NULL(emalloc(0, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(emalloc(1, 0));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, emalloc(1, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, emalloc(4, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, emalloc(8, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(emalloc(SIZE_MAX, 1));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	errno = 0;
	CU_ASSERT_PTR_NULL(emalloc(1, SIZE_MAX));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	errno = 0;
	CU_ASSERT_PTR_NULL(emalloc(SIZE_MAX, SIZE_MAX));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	
	char *mem = emalloc(128, 1);
	memset(mem, 0x7a, 128);
	
	for (size_t i = 0; i < 128; i++)
	{
		CU_ASSERT_EQUAL(mem[i], 0x7a);
	}
	
	free(mem);
}

void test_ecalloc(void)
{
	errno = 0;
	CU_ASSERT_PTR_NULL(ecalloc(0, 8));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(ecalloc(8, 0));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, ecalloc(1, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, ecalloc(1, 4));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, ecalloc(1, 8));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, ecalloc(1, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, ecalloc(4, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, ecalloc(8, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(ecalloc(SIZE_MAX, 1));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	errno = 0;
	CU_ASSERT_PTR_NULL(ecalloc(1, SIZE_MAX));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	errno = 0;
	CU_ASSERT_PTR_NULL(ecalloc(SIZE_MAX, SIZE_MAX));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	
	char *mem = ecalloc(4, 128);
	memset(mem, 0x7a, 4*128);
	
	for (size_t i = 0; i < 4*128; i++)
	{
		CU_ASSERT_EQUAL(mem[i], 0x7a);
	}
	
	free(mem);
}

void test_erealloc(void)
{
	errno = 0;
	void *m = malloc(10);
	
	CU_ASSERT_PTR_NULL(erealloc(NULL, 0, 0));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(erealloc(NULL, 0, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(erealloc(NULL, 1, 0));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, erealloc(NULL, 8, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, erealloc(malloc(20), 8, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, erealloc(malloc(20), 40, 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(erealloc(NULL, SIZE_MAX, 1));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	errno = 0;
	CU_ASSERT_PTR_NULL(erealloc(m, SIZE_MAX, 1));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	free(m);
	
	char *mem = erealloc(NULL, 128, 1);
	memset(mem, 0x7a, 128);
	
	for (size_t i = 0; i < 128; i++)
	{
		CU_ASSERT_EQUAL(mem[i], 0x7a);
	}
	
	mem = erealloc(mem, 256, 1);
	memset(&mem[128], 0x55, 128);
	
	for (size_t i = 0; i < 128; i++)
	{
		CU_ASSERT_EQUAL(mem[i], 0x7a);
	}
	for (size_t i = 128; i < 256; i++)
	{
		CU_ASSERT_EQUAL(mem[i], 0x55);
	}
	
	free(mem);
}

void test_estrndup(void)
{
	errno = 0;
	CU_ASSERT_PTR_NULL(estrndup(NULL, 5));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(estrndup(NULL, 0));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_PTR_NULL(estrndup(NULL, SIZE_MAX));
	CU_ASSERT_EQUAL(errno, 0);
	
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, estrndup("test", 1));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, estrndup("test", 10));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, estrndup("test", 0));
	CU_ASSERT_EQUAL(errno, 0);
	
	CU_ASSERT_MALLOC(CU_ASSERT_STRING_EQUAL, estrndup("test", 10), "test");
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_STRING_EQUAL, estrndup("test", SIZE_MAX), "test");
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_STRING_EQUAL, estrndup("test", 2), "te");
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_STRING_EQUAL, estrndup("test", 1), "t");
	CU_ASSERT_EQUAL(errno, 0);
	
	char tst[] = {'a', 'b', 'c', 'd'};
	CU_ASSERT_MALLOC(CU_ASSERT_STRING_EQUAL, estrndup(tst, 4), "abcd");
	CU_ASSERT_EQUAL(errno, 0);
}

void test_estrdup(void)
{
	errno = 0;
	CU_ASSERT_PTR_NULL(estrdup(NULL));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_PTR_NOT_NULL, estrdup("test"));
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_MALLOC(CU_ASSERT_STRING_EQUAL, estrdup("test"), "test");
	CU_ASSERT_EQUAL(errno, 0);
}

void test_efopen(void)
{
	errno = 0;
	CU_ASSERT_PTR_NULL(efopen("non_existant", "r"));
	CU_ASSERT_NOT_EQUAL(errno, 0);
	errno = 0;
	FILE *f = efopen("testfile.txt", "r");
	CU_ASSERT_PTR_NOT_NULL(f);
	CU_ASSERT_EQUAL(errno, 0);
	fclose(f);
}

void test_efclose(void)
{
	FILE *f = efopen("testfile.txt", "r");
	
	errno = 0;
	efclose(f);
	CU_ASSERT_EQUAL(errno, 0);
	
	efclose(NULL);
	CU_ASSERT_NOT_EQUAL(errno, 0);
}

void test_efread(void)
{
	FILE *f = efopen("testfile.txt", "r");
	
	char buf[64];
	size_t r = efread(buf, 1, sizeof buf, f);
	CU_ASSERT_STRING_EQUAL(buf, "read test\n");
	CU_ASSERT_EQUAL(r, strlen("read test\n"));
	
	fclose(f);
	
	f = efopen("testfile.txt", "a");
	fseek(f, 0, SEEK_SET);
	
	errno = 0;
	r = efread(buf, 1, sizeof buf, f);
	CU_ASSERT_EQUAL(r, 0);
	CU_ASSERT_NOT_EQUAL(errno, 0);
	
	fclose(f);
}

void test_efwrite(void)
{
	FILE *f = efopen("testfile.txt", "w+");
	
	char buf[64];
	const char *s = "this is a test\n";
	size_t r = efwrite(s, sizeof *s, strlen(s), f);
	CU_ASSERT_EQUAL(r, strlen(s));
	fflush(f);
	fseek(f, 0, SEEK_SET);
	size_t r2 = efread(buf, 1, sizeof buf, f);
	CU_ASSERT_STRING_EQUAL(buf, s);
	CU_ASSERT_EQUAL(r, strlen(s));
	CU_ASSERT_EQUAL(r2, strlen(s));
	
	fclose(f);
	
	f = efopen("testfile.txt", "r");
	
	errno = 0;
	r = efwrite(s, sizeof *s, strlen(s), f);
	CU_ASSERT_EQUAL(r, 0);
	CU_ASSERT_NOT_EQUAL(errno, 0);
	
	efclose(f);
}

void test_estrtol(void)
{
	char *b;
	
	errno = 0;
	CU_ASSERT_EQUAL(estrtol("123", NULL, 10, 0), 123);
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_EQUAL(estrtol("123   ", NULL, 10, 0), 123);
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_EQUAL(estrtol("   123", NULL, 10, 0), 123);
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_EQUAL(estrtol("   123   ", NULL, 10, 0), 123);
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_EQUAL(estrtol("123abc", NULL, 10, 0), 123);
	CU_ASSERT_EQUAL(errno, 0);
	
	CU_ASSERT_EQUAL(estrtol(NULL, NULL, 10, -1), -1);
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_EQUAL(estrtol("", NULL, 10, -1), -1);
	CU_ASSERT_EQUAL(errno, 0);
	
	CU_ASSERT_EQUAL(estrtol("400000000000000000000", NULL, 10, -1), -1);
	CU_ASSERT_NOT_EQUAL(errno, 0);
	errno = 0;
	CU_ASSERT_EQUAL(estrtol("-400000000000000000000", NULL, 10, -1), -1);
	CU_ASSERT_NOT_EQUAL(errno, 0);
	
	errno = 0;
	CU_ASSERT_EQUAL(estrtol("345test", &b, 10, 0), 345);
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_STRING_EQUAL(b, "test");
	CU_ASSERT_EQUAL(estrtol("123abc 55", &b, 10, -1), 123);
	CU_ASSERT_EQUAL(errno, 0);
	CU_ASSERT_STRING_EQUAL(b, "abc 55");
}
